#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import json
import os, time
from pyrrd.rrd import DataSource, RRA, RRD
	
def getRawValueById(id):
	url = "http://127.0.0.1/admin/requestHandler.php?r=getRawValueById&id="+str(id)
	response = urllib.urlopen(url)
	text = response.read()
	return json.loads(text)["value"]
	
def storeValueToRRD(id,value):
	filename = rrdDatabases+'value_'+str(id)+'.rrd'
	#check if file exists and if not create
	dataSources = []
	roundRobinArchives = []
	dataSource = DataSource(dsName='value'+str(id),dsType='GAUGE',heartbeat=40)
	dataSources.append(dataSource)
	roundRobinArchives.append(RRA(cf='AVERAGE',xff=0.5,steps=1,rows=1080))
	roundRobinArchives.append(RRA(cf='AVERAGE',xff=0.5,steps=5,rows=1296))
	roundRobinArchives.append(RRA(cf='AVERAGE',xff=0.5,steps=25,rows=1210))
	roundRobinArchives.append(RRA(cf='AVERAGE',xff=0.5,steps=120,rows=1120))
	roundRobinArchives.append(RRA(cf='AVERAGE',xff=0.5,steps=1440,rows=1120))
	roundRobinArchives.append(RRA(cf='MIN',xff=0.5,steps=12,rows=2400))
	roundRobinArchives.append(RRA(cf='MAX',xff=0.5,steps=12,rows=2400))
	myRRD = RRD(filename,ds=dataSources,rra=roundRobinArchives,step=20)
	if not os.path.isfile(filename):
		myRRD.create()
	if value['rawValue'] == True:
		value['rawValue'] = int(1)
	if value['rawValue'] == False:
		value['rawValue'] = int(0)
	myRRD.bufferValue(time.time(),value['rawValue'])
	myRRD.update()
	#readRRD = RRD(filename, mode="r")
	#print readRRD.ds[0].getData()["value"]
	#print myRRD.info();
		
	return 0
	
def getAllValuesToBeStored():
	url = "http://127.0.0.1/admin/requestHandler.php?r=getAllValuesForRRD"
	response = urllib.urlopen(url)
	text = response.read()
	try:
		list = json.loads(text)
	except:
#		print text
		list = []
	return list
	

print urllib.urlopen("http://127.0.0.1/admin/requestHandler.php?r=checkLimits").read()

tmp = getAllValuesToBeStored()
if len(tmp) != 0:
	rrdDatabases = tmp['databases']
	rrdScripts = tmp['scripts']
	values = tmp['values']
	for i in values:
		storeValueToRRD(i,values[i])
	print "stored values"
else:
	print "error in response, skipping"
