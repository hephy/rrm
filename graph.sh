#!/usr/bin/env bash
#rrdtool graph /var/www/html/output/rrd_scripts/test.png -a PNG --start -15000 -t "Test" -w 800 -h 200 \
#DEF:t02=/var/www/html/output/rrd_databases/value_2.rrd:value2:AVERAGE \
#DEF:t16=/var/www/html/output/rrd_databases/value_16.rrd:value16:AVERAGE \
#DEF:t31=/var/www/html/output/rrd_databases/value_31.rrd:value31:AVERAGE \
#DEF:t38=/var/www/html/output/rrd_databases/value_38.rrd:value38:AVERAGE \
#DEF:t39=/var/www/html/output/rrd_databases/value_39.rrd:value39:AVERAGE \
#DEF:t40=/var/www/html/output/rrd_databases/value_40.rrd:value40:AVERAGE \
#DEF:t41=/var/www/html/output/rrd_databases/value_41.rrd:value41:AVERAGE \
#LINE1:t02#ff0000:" Value 02" \
#LINE1:t16#00ff00:" Value 16" \
#LINE1:t31#0000ff:" Value 31" \
#LINE1:t38#00ffff:" Value 38" \
#LINE1:t39#ffff00:" Value 39" \
#LINE1:t40#cccccc:" Value 40" \
#LINE1:t41#888888:" Value 41" \

rrdtool graph /var/www/html/output/rrd_scripts/tempout_6h.png -a PNG --start -21600 -t "Frischluftventilation (6h)" --vertical-label "°C" -w 800 -h 150 \
--alt-autoscale --alt-y-grid \
DEF:g1=/var/www/html/output/rrd_databases/value_39.rrd:value39:AVERAGE \
DEF:gmin=/var/www/html/output/rrd_databases/value_39.rrd:value39:MIN \
DEF:gmax=/var/www/html/output/rrd_databases/value_39.rrd:value39:MAX \
VDEF:g1l=g1,LAST \
VDEF:g1a=g1,AVERAGE \
VDEF:gmina=gmin,MINIMUM \
VDEF:gmaxa=gmax,MAXIMUM \
LINE1:g1#FF5555:" Ansaugtemperatur aussen" \
GPRINT:g1l:"LAST\: %5.1lf °C" \
GPRINT:g1a:"AVG\: %5.2lf °C" \
GPRINT:gmina:"MIN\: %5.1lf °C" \
GPRINT:gmaxa:"MAX\: %5.1lf °C \n" \
DEF:g3=/var/www/html/output/rrd_databases/value_38.rrd:value38:AVERAGE \
DEF:g3min=/var/www/html/output/rrd_databases/value_38.rrd:value38:MIN \
DEF:g3max=/var/www/html/output/rrd_databases/value_38.rrd:value38:MAX \
VDEF:g3l=g3,LAST \
VDEF:g3a=g3,AVERAGE \
VDEF:g3mina=g3min,MINIMUM \
VDEF:g3maxa=g3max,MAXIMUM \
LINE1:g3#000000:" Ausblastemperatur innen" \
GPRINT:g3l:"LAST\: %5.1lf °C" \
GPRINT:g3a:"AVG\: %5.2lf °C" \
GPRINT:g3mina:"MIN\: %5.1lf °C" \
GPRINT:g3maxa:"MAX\: %5.1lf °C \n" \
LINE1:g1a#FF5555 \
LINE1:g3a#000000 \

# 6 Stunden - Temperatur von Main und small cleanroom und fresh air supply
nice -n 19 rrdtool graph /var/www/html/output/rrd_scripts/temp_6h.png -a PNG --start -21600 -t "Innentemperaturen (6h)" --vertical-label "°C" -w 800 -h 200 \
 --rigid \
 --alt-y-grid \
DEF:g1=/var/www/html/output/rrd_databases/value_2.rrd:value2:AVERAGE \
DEF:g2=/var/www/html/output/rrd_databases/value_31.rrd:value31:AVERAGE \
DEF:g1min=/var/www/html/output/rrd_databases/value_2.rrd:value2:MIN \
DEF:g1max=/var/www/html/output/rrd_databases/value_2.rrd:value2:MAX \
DEF:g2min=/var/www/html/output/rrd_databases/value_31.rrd:value31:MIN \
DEF:g2max=/var/www/html/output/rrd_databases/value_31.rrd:value31:MAX \
VDEF:g1l=g1,LAST \
VDEF:g2l=g2,LAST \
VDEF:g1a=g1,AVERAGE \
VDEF:g2a=g2,AVERAGE \
VDEF:g1mina=g1min,MINIMUM \
VDEF:g1maxa=g1max,MAXIMUM \
VDEF:g2mina=g2min,MINIMUM \
VDEF:g2maxa=g2max,MAXIMUM \
LINE1:g1#ff0000:" Main Cleanroom  " \
GPRINT:g1l:"LAST\: %5.1lf °C" \
GPRINT:g1a:"AVG\: %5.2lf °C" \
GPRINT:g1mina:"MIN\: %5.1lf °C" \
GPRINT:g1maxa:"MAX\: %5.1lf °C\n" \
LINE1:g2#34a706:" Small Cleanroom " \
GPRINT:g2l:"LAST\: %5.1lf °C" \
GPRINT:g2a:"AVG\: %5.2lf °C" \
GPRINT:g2mina:"MIN\: %5.1lf °C" \
GPRINT:g2maxa:"MAX\: %5.1lf °C \n" \
LINE2:22.5#0000FF \
LINE2:g1a#ff0000 \
LINE2:g2a#34a706 \
> /dev/null

###############################################
# Room Over Pressure
nice -n 19 rrdtool graph /var/www/html/output/rrd_scripts/diffpressure_6h.png -a PNG --start -21600 -t "Überdruck (6h)" --vertical-label "Pa" -w 800 -h 150 \
--alt-autoscale --alt-y-grid \
DEF:g1=/var/www/html/output/rrd_databases/value_41.rrd:value41:AVERAGE \
DEF:gmin=/var/www/html/output/rrd_databases/value_41.rrd:value41:MIN \
DEF:gmax=/var/www/html/output/rrd_databases/value_41.rrd:value41:MAX \
VDEF:g1l=g1,LAST \
VDEF:g1a=g1,AVERAGE \
VDEF:gmina=gmin,MINIMUM \
VDEF:gmaxa=gmax,MAXIMUM \
LINE1:g1#000000:" Room Over Pressure Pa" \
GPRINT:g1l:"LAST\: %5.2lf Pa" \
GPRINT:g1a:"AVG\: %5.2lf °C" \
GPRINT:gmina:"MIN\: %5.2lf Pa " \
GPRINT:gmaxa:"MAX\: %5.2lf Pa\n" \
LINE2:g1a#FF0000 \
> /dev/null


##############################################
# Vakuum von SMC Sensor
nice -n 19 rrdtool graph /var/www/html/output/rrd_scripts/smcvak_6h.png -a PNG --start -21600 -t "Vakuum (6h)" --vertical-label "bar" -w 800 -h 150 \
DEF:g1=/var/www/html/output/rrd_databases/value_40.rrd:value40:AVERAGE \
DEF:gmin=/var/www/html/output/rrd_databases/value_40.rrd:value40:MIN \
DEF:gmax=/var/www/html/output/rrd_databases/value_40.rrd:value40:MAX \
VDEF:g1l=g1,LAST \
VDEF:g1a=g1,AVERAGE \
VDEF:gmina=gmin,MINIMUM \
VDEF:gmaxa=gmax,MAXIMUM \
LINE1:g1#000000:" Vacuum bar" \
GPRINT:g1l:"LAST\: %5.2lf bar" \
GPRINT:g1a:"AVG\: %5.2lf °C" \
GPRINT:gmina:"MIN\: %5.2lf bar" \
GPRINT:gmaxa:"MAX\: %5.2lf bar\n" \
LINE2:g1a#FF0000 \
> /dev/null

##############################################
# Druckluft Kompressor
nice -n 19 rrdtool graph /var/www/html/output/rrd_scripts/smcpressure_6h.png -a PNG --start -21600 -t "Druckluft Kompressor (6h)" --vertical-label "bar" -w 800 -h 150 \
--alt-autoscale --alt-y-grid \
DEF:g1=/var/www/html/output/rrd_databases/value_16.rrd:value16:AVERAGE \
DEF:gmin=/var/www/html/output/rrd_databases/value_16.rrd:value16:MIN \
DEF:gmax=/var/www/html/output/rrd_databases/value_16.rrd:value16:MAX \
VDEF:g1l=g1,LAST \
VDEF:g1a=g1,AVERAGE \
VDEF:gmina=gmin,MINIMUM \
VDEF:gmaxa=gmax,MAXIMUM \
LINE1:g1#000000:" Compr. Air Pressure bar" \
GPRINT:g1l:"LAST\: %5.2lf bar" \
GPRINT:g1a:"AVG\: %5.2lf °C" \
GPRINT:gmina:"MIN\: %5.2lf bar" \
GPRINT:gmaxa:"MAX\: %5.2lf bar\n" \
LINE2:g1a#FF0000 \
> /dev/null
