<?php

require_once "Action.php";

/**
 * User: Benedikt Würkner
 */
class LogAction extends Action
{
	private $logLevel = 3;
	private $logMessage = "";
	
	public function __construct($limitId)
	{
		parent::__construct($limitId);
		$this->triggerDelay = 0;
	}
	
	public function performAction()
	{
		if(!$this->canBeTriggered()) return true;  //Return if the action was already triggered
		//Get Value from Limit
		global $defaultLogger;
		$limit = Limit::getLimitById($this->limit_id,0);
		$valueId = $limit->getValueId();
		$value = Value::fromDatabase($valueId);
		if($limit->getLimits()["lowerLimit"] === null){  //Is a boolean, outputting limit is senseless
			$valueString = "";
		}else{
			$valueString = $limit->getLimits()["lowerLimit"]." < ".$value->getFormattedValue()." < ".$limit->getLimits()["upperLimit"].".";
			
		}
		$defaultLogger->log($this->logMessage." ".$valueString,$this->logLevel);
		$this->setTriggered();
	}
	
	public function getConfiguration()
	{
		global $defaultLogger;
		$return  = "Log: ";
		$return .= generateSelect("logLevel",$defaultLogger->getLogLevels(),$this->logLevel);
		$return .= "<input type='text' value='".$this->logMessage."' name='logMessage' />";
		$return .= "<input type='hidden' value='log' name='action' />";
		$return .= "<input type='button' value='x' name='removeAction' onClick='removeAction(this)'/>";
		return $return;
	}
	
	public function saveSettings($data)
	{
		$this->logLevel = $data["logLevel"];
		$this->logMessage = $data["logMessage"];
	}
}
