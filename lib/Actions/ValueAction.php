<?php

require_once "Action.php";

/**
 * User: Benedikt Würkner
 */
class ValueAction extends Action
{
	private $valueId;
	private $targetValue;
	
	public function __construct($limitId)
	{
		parent::__construct($limitId);
		$this->triggerDelay = 0;
	}
	
	public function performAction()
	{
		if(!$this->canBeTriggered()) return true;  //Return if the action was already triggered
		$value = Value::fromDatabase($this->valueId);
//		global $defaultLogger;
//		$defaultLogger->log("Set Value to ".$this->targetValue,2);
		
		$value->setValue($this->targetValue);
		$value->storeToDatabase();
		$this->setTriggered();
	}
	
	public function getConfiguration()
	{
		$return  = "Value: ";
		$return .= generateSelect("valueId",Value::getAllWriteableValues(),$this->valueId);
		$return .= "<input type='text' value='".$this->targetValue."' name='targetValue' />";
		$return .= "<input type='hidden' value='setValue' name='action' />";
		$return .= "<input type='button' value='x' name='removeAction' onClick='removeAction(this)'/>";
		
		return $return;
	}
	
	public function saveSettings($data)
	{
		$this->valueId = $data["valueId"];
		$this->targetValue = $data["targetValue"];
	}
}
