<?php

require_once "Action.php";

/**
 * User: Benedikt Würkner
 */
class ColorAction extends Action
{
	private $text = "#000000";
	private $background = "#00ff00";
	
	public function __construct($limitId)
	{
		parent::__construct($limitId);
		$this->triggerDelay = 0;
	}
	
	public function performAction()
	{
		if(!$this->canBeTriggered()) return true;  //Return if the action was already triggered
		
		//Get Value from Limit
		$limit = Limit::getLimitById($this->limit_id,0);
		$valueId = $limit->getValueId();
		$value = Value::fromDatabase($valueId);
		
		//Set Value colors according to the set parameters. 
		$value->setColors($this->background,$this->text);
		global $defaultLogger;
		$valueString = $limit->getLimits()["lowerLimit"]." < ".$value->getFormattedValue()." < ".$limit->getLimits()["upperLimit"].".";
		$defaultLogger->logNotice("Colors of \"".$value->getName()."\" (".$valueId.") were set because the limit was reached. ".$valueString);
		$this->setTriggered();
	}
	
	public function getConfiguration()
	{
		$return  = "Color: ";
		$return .= "Text-color: <input type='color' value='".$this->text."' name='text'  onchange='updatePreview(this)'/>";
		$return .= "Background-color: <input type='color' value='".$this->background."' name='background' onchange='updatePreview(this)' />";
		$return .= "<input type='text' value='12.5 V' name='preview' disabled='disabled' class='preview' style='color:".$this->text."; background-color:".$this->background."' />";
		$return .= "<input type='hidden' value='color' name='action' />";
		$return .= "<input type='button' value='x' name='removeAction' onClick='removeAction(this)'/>";
		return $return;
	}
	
	public function saveSettings($data)
	{
		$this->text = $data["text"];
		$this->background = $data["background"];
	}
}
