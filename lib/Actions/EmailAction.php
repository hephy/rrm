<?php

require_once "Action.php";

/**
 * User: Benedikt Würkner
 */
class EmailAction extends Action
{
	private $recipient="";
	private $subject="";
	private $body="";
	
	public function __construct($limitId)
	{
		parent::__construct($limitId);
	}
	
	public function performAction()
	{
		if(!$this->triggerDelayPassed()) return true;  //Return if the trigger delay hasn't passed yet
		mail($this->recipient,$this->subject,$this->body);
		
		$this->setTriggered();
	}
	
	public function getConfiguration()
	{
		$return  = "Email: ";
		$return .= "<input type='text' value='".$this->recipient."' name='recipient' placeholder='info@server.com' />";
		$return .= "<input type='text' value='".$this->subject."' name='subject' placeholder='Subject' />";
		$return .= "<input type='text' value='".$this->body."' name='body' placeholder='Message' />";
		$return .= "<input type='hidden' value='email' name='action' />";
		$return .= "<input type='button' value='x' name='removeAction' onClick='removeAction(this)'/>";
		return $return;
	}
	
	public function saveSettings($data)
	{
		$this->recipient = $data["recipient"];
		$this->subject = $data["subject"];
		$this->body = $data["body"];
	}
}
