<?php

/**
 * User: Benedikt Würkner
 */

/**
 * Class Action
 * Possible Actions: Email, Change Color, Set Value, ...
 * Set Limit for how often an action can be executed or set a time delay before an action can be triggered again.
 */
abstract class Action
{
	protected $lastTriggerTime = 0;
	protected $triggerDelay = 1800;
	protected $triggered = false;
	protected $id;
	protected $limit_id;
	
	/**
	 * Action constructor.
	 * @param int $limitId The Id of the limit this action belongs to
	 */
	public function __construct($limitId)
	{
		$this->limit_id = $limitId;
	}
	
	/**
	 * Tells the system to trigger the action which is only defined in the child classes. 
	 * @return mixed
	 */
	abstract function performAction();
	
	/**
	 * Returns the HTML-Elements required to configure each child class
	 * @return mixed
	 */
	abstract function getConfiguration();
	
	/**
	 * Stores the settings given in the HTML-Elements in the object for future use. 
	 * @param $data array An Array containing the expected configuration values
	 * @return mixed
	 */
	abstract function saveSettings($data);
	
	/**
	 * Sets the internal trigger time value to the current timestamp
	 */
	function setTriggerTime(){
		$this->lastTriggerTime = microtime(true);
	}
	
	/**
	 * Trigger delay has passed, action can be triggered again from a time delay standpoint
	 * @return bool Trigger delay has passed, action can be triggered again
	 */
	function triggerDelayPassed(){
		return (microtime(true)>$this->lastTriggerTime+$this->triggerDelay);
	}
	
	/**
	 * Defines if an action can be triggered. 
	 * @return bool Action can be triggered
	 */
	function canBeTriggered(){
#		debug(get_class($this));
#		debug($this->triggerDelayPassed());
#		debug($this->triggered);
		return ($this->triggerDelayPassed() && !$this->triggered);
	}
	
	/**
	 * Sets this action as triggered
	 */
	function setTriggered(){
		$this->triggered = true;
		$this->setTriggerTime();
	}
	
	/**
	 * Sets this action as not triggered
	 */
	function resetTrigger(){
		$this->triggered = false;
	}
}
