<?php

/**
 * Logging class that can be used to log everything. The system internally stores the log in the correct locations (e.g. file, database, both) according to the setting defined during initialization 
 * 
 * User: Benedikt Würkner
 */

/**
 * Class Logger
 */
class Logger
{
	private $logToDb = false;
	private $db = null;
	private $dbIdentifier = "default";
	private $logToFile = true;
	private $filePath = "";
	
	/**
	 * Logger constructor.
	 * @param string $logfilePath Path to the logfile
	 * @param SQLiteDatabase $dbconnection Reference to a database connection
	 * @param bool $logToFile Defines if the log messages should be written to a log file
	 * @param bool $logToDatabase Defines if the log messages should be written to a log table
	 * @param string $dbIdentifier Defines the identifier of this logger in the database (Since always the same table is used)
	 * @throws Exception
	 */
	public function __construct($logfilePath,&$dbconnection=null,$logToFile=true,$logToDatabase=false,$dbIdentifier="default")
	{
		//Check if system should write to logfile
		if($logToFile){
			//Check if logfile exists and is writable
			if(file_exists($logfilePath)){
				$this->logToFile = true;
				$this->filePath = $logfilePath;
			}elseif(touch($logfilePath)){
				$this->logToFile = true;
				$this->filePath = $logfilePath;
			}else{
				$this->logToFile = false;
			}
		}else{
			$this->logToFile = false;
		}
		
		
		//Check if system should write to database
		if($logToDatabase){
			//Check if database is valid and has table "log" with the correct cols
			if(is_a($dbconnection,"SQLite3")){
				$stmt = $dbconnection->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name=:name;");
				$stmt->bindValue(':name', "log", SQLITE3_TEXT);
				$result = $stmt->execute();
				if($result->fetchArray(SQLITE3_BOTH)){
					$this->logToDb = true;
					$this->db = &$dbconnection;
					$this->dbIdentifier = $dbIdentifier;
				}else{
					$this->logToDb = false;
				}
			}else{
				$this->logToDb = false;
				$this->db = null;
			}
		}
		
		if(!$this->logToFile && !$this->logToDb){
			throw new Exception("Couldn't initialize logger, no logging possible, aborting");	
		}
	}
	
	/**
	 * Internal function to log something
	 * @param int $level Level of the log
	 * @param string $text Text to be logged
	 */
	private function _log($level,$text){
		if($this->logToDb){
			$stmt = $this->db->prepare("INSERT INTO log (id,level,message,timestamp,identifier) VALUES (:id,:level,:message,:timestamp,:identifier)");
			$stmt->bindValue(':id', null, SQLITE3_INTEGER);
			$stmt->bindValue(':level', $level, SQLITE3_INTEGER);
			$stmt->bindValue(':message', $text, SQLITE3_TEXT);
			$stmt->bindValue(':timestamp', microtime(true), SQLITE3_INTEGER);
			$stmt->bindValue(':identifier', $this->dbIdentifier, SQLITE3_TEXT);
			$stmt->execute();
		}
		if($this->logToFile){
			switch ($level){
				case 1:
					$msg1 = "ERROR:   ";
					break;
				case 2:
					$msg1 = "WARNING: ";
					break;
				case 3:
				default:
					$msg1 = "NOTICE:  ";
					break;
			}
			file_put_contents($this->filePath,date("d.m.Y - H:i:s",microtime(true))."\t".$msg1.$text."\n",FILE_APPEND);
		}
	}
	
	/**
	 * Shortcut for Error Logging
	 * @param string $text Text to be logged
	 */
	public function logError($text){
		$this->_log(1,$text);
	}
	
	/**
	 * Shortcut for Warning Logging
	 * @param string $text Text to be logged
	 */
	public function logWarning($text){
		$this->_log(2,$text);
	}
	
	/**
	 * Shortcut for Notice Logging
	 * @param string $text Text to be logged
	 */
	public function logNotice($text){
		$this->_log(3,$text);
	}
	
	/**
	 * Log something
	 * @param string $text
	 * @param int $level
	 */
	public function log($text,$level=1){
		switch ($level){
			case 1:
				$this->logError($text);
				break;
			case 2:
				$this->logWarning($text);
				break;
			case 3:
			default:
				$this->logNotice($text);
				break;
		}
	}
	
	/**
	 * Retrieve log messages
	 * @param int $level Level of the log requested, use 0 to get all levels
	 * @param int $rows Number of rows to be returned at max, use -1 to return all
	 * @return array
	 */
	public function getLog($level = 3,$rows = 30){
		global $db;
	
		if($level==0){
			//Display all
			$stmt = $db->prepare('SELECT id,message, timestamp, identifier,level FROM `log` ORDER BY timestamp DESC LIMIT :limit;');
		}
		elseif($level >=4 && $level <=6) {

            switch ($level)
            {
                case 4:
                    $level1 = 2;
                    $level2 = 3;
                    break;
                case 5:
                    $level1 = 1;
                    $level2 = 3;
                    break;
                case 6:
                    $level1 = 1;
                    $level2 = 2;
                    break;
            }

            $stmt = $db->prepare('SELECT id, message, timestamp, identifier,level FROM `log` WHERE level = :level1 OR level = :level2 ORDER BY timestamp DESC LIMIT :limit;');
            $stmt->bindParam(":level1",$level1,SQLITE3_INTEGER);
            $stmt->bindParam(":level2",$level2,SQLITE3_INTEGER);
        }
		else{
			$stmt = $db->prepare('SELECT id, message, timestamp, identifier,level FROM `log` WHERE level = :level ORDER BY timestamp DESC LIMIT :limit;');
			$stmt->bindParam(":level",$level,SQLITE3_INTEGER);
		}
		$stmt->bindParam(":limit",$rows,SQLITE3_INTEGER);
		$result = $stmt->execute();
		$return = array();
		$logLevels = $this->getLogLevels();
		while($row = $result->fetchArray(SQLITE3_BOTH)){
			$row["levelName"] = $logLevels[$row["level"]];
			$return[] = $row;
		}
		return $return;
	}
	
	/**
	 * Returns all existing loglevels
	 * @return array Array of all possible loglevels
	 */
	public function getLogLevels(){
		return array(1=>"ERROR",2=>"WARNING",3=>"NOTICE");
	}
	
}
