<?php

/**
 * GPIO pins interfacing class
 */

require_once "DataSource.php";
require_once "../lib/php-gpio/vendor/autoload.php";
use PhpGpio\Gpio;

/**
 * Class GpioPHP
 */
class GpioPHP implements DataSource
{
	private $conn;
	
	/**
	 * GpioPHP constructor.
	 */
	function __construct()
	{
		$this->conn = new GPIO();
	}
	
	/**
	 * Retrieves the value from the datasource
	 * @param string $address
	 * @return false|string
	 */
	function getValueFromAddress($address)	{
		return $this->conn->input($address);
	}
	
	/**
	 * Defines a pin as input or output
	 * @param int $pinId Id of the pin to be configured
	 * @param string $direction "in" or "out" giving the direction of a gpio pin 
	 * @return bool
	 */
	function setup($pinId,$direction){
		return $this->conn->setup($pinId,$direction);
	}
	
	/**
	 * Returns the last error message
	 * @return string 
	 */
	function getError(){
		return $this->conn->lastError;
	}
	
	/**
	 * Tries to write a value to the pin (0 or 1)
	 * @param string $address 
	 * @param bool $value
	 * @return mixed
	 */
	function writeValueToAddress($address,$value){
		return $this->conn->output($address,$value);
	}
	
	/**
	 * Returns all pins and their currently set direction (if exported)
	 * @return array
	 */
	function getAllPins(){
		$allPins = $this->conn->getAllPins();
		
		$pins = array();
		foreach($allPins as $pinId){
			$direction = ($this->conn->isExported($pinId))?$this->conn->currentDirection($pinId):"in";
			$pins[$pinId] = array("id"=>$pinId,"direction"=>$direction);
		}
		return $pins;
	}
	
	/**
	 * Save the current GPIO configuration into a file that can then be executed by the system at startup to correctly set the pins. 
	 */
	function saveConfiguration(){
		$allPins = $this->conn->getHackablePins();
		$return = "#! /bin/bash\n";
		foreach($allPins as $pinId){
			if($this->conn->isExported($pinId)){
				$return .= "echo \"".$pinId."\" > /sys/class/gpio/export \n";
				$return .= "echo \"".$this->conn->currentDirection($pinId)."\" > /sys/class/gpio/gpio".$pinId."/direction \n";
			}
		}
		file_put_contents($GLOBALS["basePath"]."gpio.sh",$return);
		chmod($GLOBALS["basePath"]."gpio.sh",0775);
		return true;
		}
}
