<?php

/**
 * User: Benedikt Würkner
 * 
 * This class describes a locally used value that can be written and read out by the system just like every other value. 
 * One of the usages of these values is the flag if an alarm is silenced, but this value can be used for any other purpose as well. 
 */

/**
 * Class LocalValue
 */
class LocalValue implements DataSource {
	
	private $value = 0;
	
	/**
	 * Stores the value locally
	 * @param string $address
	 * @param mixed $val
	 */
	function writeValueToAddress($address, $val)
	{
		$this->value = $val;
	}
	
	/**
	 * Retrieves the locally stored value
	 * @param $address
	 * @return int
	 */
	function getValueFromAddress($address)
	{
		return $this->value;
	}
}
