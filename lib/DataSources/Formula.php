<?php

/**
 * Formula class
 * 
 * This class represents a formula applied to one or multiple other values (which in and of themselves can of course again be formulas)
 * A formula consists of numeric values, the usual mathematical symbols like +,-,*,/ and references to values given in curly braces. 
 * {1} for example would reference the value 1.  
 */

require_once "DataSource.php";
require_once "EvalMath.php";
require_once "FormulaExtended.php";

/**
 * Datasource representing a formula
 * Class Formula
 */
class Formula implements DataSource
{
	private $rawFormula;
	private $convertedFormula;
	private $requiredValues; //Contains a list of Value IDs that are used as variables in this Formula
	
	/**
	 * Formula constructor.
	 * @param $formula string A string representation of the formula that should be used. 
	 */
	function __construct($formula)
	{
		$this->rawFormula = $formula;
		preg_match_all("/(?<={)(.*?)(?=})/",$formula,$this->requiredValues);
		$this->requiredValues = array_unique($this->requiredValues[0]); //Remove duplicates and only store the first level of the array
		$this->convertedFormula = $this->rawFormula;
		foreach($this->requiredValues as $valueId){
			$this->convertedFormula = str_replace("{".$valueId."}","v".$valueId,$this->convertedFormula);
		}
	}
	
	/**
	 * Checks if a given Formula is valid like do the ids used exist in the system, is there no division by zero and so on. 
	 * 
	 * @param $formula string The Formula string to be checked for validity
	 * @return bool True if the formula is valid, false if not
	 */
	public static function checkFormula($formula){
        if ($formula[0] == '[' && $formula[strlen($formula)-1] == ']')
        {
            return (new FormulaExtended())->checkFormulaExtended($formula);
        }

		preg_match_all("/(?<={)(.*?)(?=})/",$formula,$requiredValues);
		$requiredValues = array_unique($requiredValues[0]); //Remove duplicates and only store the first level of the array
		$convertedFormula = $formula;
		foreach($requiredValues as $valueId){
			$convertedFormula = str_replace("{".$valueId."}","v".$valueId,$convertedFormula);
		}
		//Now that we have the converted Formula check if all the values used really are double values that can be used in formulas 
		$m = new EvalMath();
		foreach($requiredValues as $valueId){
			$value = Value::fromDatabase($valueId);
			if($value->getValueType() != "double"){
				return array("error"=>true,"desc"=>"Value '".$valueId."' is no double");
			}
			$m->e("v".$valueId." = ".$value->getValue());  //Set variables for the EvalMath class
		}
		//All values are double values, check if some other formula error occurs
		$m->suppress_errors = true;
		if($m->e($convertedFormula) === false){
			return array("error"=>true,"desc"=>"There seems to be an error in your formula. Probably a division by 0. "); //Some error in the formula, probably division by zero
		}else{
			return array("error"=>false,"desc"=>"No Error found"); //Evaluation returned a value, everything seems to be ok
		}
	}
	
	/**
	 * Returns the value of the formula
	 * @return bool|float|mixed|null|Value
	 */
	private function value(){
        if ($this->rawFormula[0] == '['  && $this->rawFormula[strlen($this->rawFormula)-1] == ']')
        {
            return (new FormulaExtended())->value($this->rawFormula);
        }

		$m = new EvalMath();
		foreach($this->requiredValues as $valueId){
			$value = Value::fromDatabase($valueId);
			$m->e("v".$valueId." = ".$value->getValue());  //Set variables for the EvalMath class
		}
		$value = $m->e($this->convertedFormula);
		$value = (floatval($value) != 0)?floatval($value):$value;
		return $value;
	}
	
	/**
	 * Returns the formula definition
	 * @return string Raw Formula
	 */
	public function getRawFormula(){
		return $this->rawFormula;
	}
	
	/**
	 * Pseudo request definition that returns the value regardless of the address
	 * @param string $address Nonsensical parameter, only there because required by the interface
	 * @return bool|float|mixed|null|Value
	 */
	function getValueFromAddress($address){
		return $this->value();
	}
	
	/**
	 * @param $address string irrelevant 
	 * @param $value integer irrelevant
	 * @return bool false - always returns false since a formula value cannot be written
	 */
	function writeValueToAddress($address,$value){
		return false;
	}
	
}
