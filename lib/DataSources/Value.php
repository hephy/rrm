<?php

/**
 * Main Value Class
 * 
 * This Class represents every value used by the system and has its own abilities to store itself and reload itself from the database
 * 
 */


/**
 * Base definition of a value to be used within the Program
 */
class Value
{
	
	private $isWriteable = false;
	private $datasource;
	private $address;
	private $id;
	private $isTemp = false;
	private $name;
	private $valueType;
	private $configData = array(
			"unit"=>"",
			"number_of_decimals"=>2,
			"log_in_rrd"=>0
		);
	
	private $colors = array();
	private $valueNames = array();
	
	
	function __construct()
	{
		//allocate stuff if necessary
	}
	
	function __wakeup()
	{
		// TODO: restore connection to datasource - Doesn't appear to be necessary 
		// Appears to be working without a problem for 1Wire as it just restores the whole connection object
		// Test for KNX and GPIO - Also works
	}
	
	/**
	 * Initiates a value from the Database
	 * @param int $id Variable giving the id of the Value in the Database
	 * @return Value Returns an object of this Class
	 */
	public static function fromDatabase($id){
		global $db;

		//TODO: Check if id exists in database
		$stmt = $db->prepare('SELECT object FROM `values` WHERE id=:id');
		$stmt->bindValue(':id', $id, SQLITE3_INTEGER);
		$result = $stmt->execute();
		$return = $result->fetchArray();
		$value = Value::dec($return["object"]);
		if(is_a($value,"Value")){
			return $value;
		}else{
			return new Value();
		}
		
	}
	
	/**
	 * Creates a new value and stores it in the database thereby associating a new ID with it. 
	 * At first it checks if the address is already associated in the database and if yes instead loads this
	 * @param String $name 
	 * @param DataSource $datasource A DataSource object that shall be used to access the data via the correct interfaces
	 * @param String $address A String representation of the Address of the DataSource in a format the DataSource understands
	 * @param bool $isWriteable default = false
	 * @param string $valueType The type of the value
	 * @return Value Returns an object of this Class
	 */
	public static function create($name,DataSource &$datasource,$address,$isWriteable=false,$valueType=false){
		//TODO: Check if this address is already used in the database
		$instance = new self();
		if($instance->existsInDatabase($name,$address)){
			$instance = $instance->fromDatabase($instance->getIdFromAddress($address));
		}else{
			if(is_a($datasource,"LocalValue")){
				$instance->datasource = clone $datasource;
			}else{
				$instance->datasource = &$datasource;
			}
			$instance->name = $name;
			$instance->address = $address;
			$instance->setWriteable($isWriteable);
			if($valueType===false){
				$instance->recognizeValueType();
			}else{
				$instance->setValueType($valueType);
			}
			$instance->storeToDatabase();
		}
		return $instance;
	}
	
	/**
	 * Temporary Value object, not stored in database
	 * @param String $name
	 * @param DataSource $datasource A DataSource object that shall be used to access the data via the correct interfaces
	 * @param String $address A String representation of the Address of the DataSource in a format the DataSource understands
	 * @param bool $isWriteable default = false
	 * @param string $valueType The type of the value
	 * @return Value Returns an object of this Class
	 */
	public static function createTemp($name,DataSource &$datasource,$address,$isWriteable=false,$valueType="NULL"){
		$instance = new self();
		$instance->datasource = &$datasource;
		$instance->name = $name;
		$instance->address = $address;
		$instance->isWriteable = $isWriteable;
		if($valueType == "NULL"){
			$instance->recognizeValueType();
		}else{
			$instance->valueType = $valueType;
		}
		$instance->isTemp = true;
		return $instance;
	}
	
	/**
	 * Encodes a given variable as base64 after serializing it.
	 * @param mixed $var the variable to be encoded
	 * @return string a base64 string encoding the original variable
	 */
	public static function enc($var){
		return base64_encode(serialize($var));
	}
	
	/**
	 * Decodes a base64 encoded object
	 *
	 * @param string $var a base64 encoded variable
	 * @return mixed an object containing whatever the original was.
	 */
	public static function dec($var){
		return unserialize(base64_decode($var));
	}
	
	public static function addressIsSet($address){
		return is_int(Value::getIdFromAddress($address));
	}
	
	public function getAddress(){
		return $this->address;
	}
	
	public function setAddress($address){
		$this->address = $address;
	}
	
	public static function nameIsSet($name){
		global $db;
		$stmt = $db->prepare('SELECT id,address FROM `values` WHERE name LIKE :name');
		$stmt->bindValue(':name', $name, SQLITE3_TEXT);
		$result = $stmt->execute();
		
		$return = array();
		while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
			$return[] = $row;
		}
		return (count($return) > 0);
	}
	
	public static function updateName($id,$name){
		global $db;
		$obj = Value::fromDatabase($id);
		$obj->setName($name);
		$stmt = $db->prepare('UPDATE `values` SET `name`=:name, `object`=:object WHERE `id`=:id');
		$stmt->bindValue(':name', $name, SQLITE3_TEXT);
		$stmt->bindValue(':object', Value::enc($obj), SQLITE3_TEXT);
		$stmt->bindValue(':id', $id, SQLITE3_INTEGER);
		$stmt->execute();
		
	}
	
	public function updateDatasource(DataSource &$datasource){
		$this->datasource = $datasource;
	}
	
	public static function getIdFromAddress($address){
		global $db;
#		$address = "%\"".$address."\";"; //Add quotes and a semicolon so the format matches SQLITE3 O.o
		$stmt = $db->prepare('SELECT id,address FROM `values` WHERE address LIKE :address');
		$stmt->bindValue(':address', $address, SQLITE3_TEXT);
		$result = $stmt->execute();
		$return = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$return[] = $row;
		}
		if(count($return)==0){
			//Couldn't find address, return false to signal that none was found
			return false;
		}elseif(count($return)==1){
			//Found exactly one result, return the id
			return $return[0]["id"];
		}else{
			//Found the address multiple times, this shouldn't happen so throw an error ...
			debug("THIS SHOULDN'T HAPPEN!!!!");
			debug($return);
			return true;
		}
	}
	
	/**
	 * @param string $name The Name of the variable (should be unique)
	 * @param string $address The address of the variable in the format accepted by the DataSource
	 * @return bool
	 */
	public static function existsInDatabase($name,$address)
	{
		global $db;
#		$address = "%\"".$address."\";"; //Add quotes and a semicolon so the format matches SQLITE3 O.o
		$stmt = $db->prepare('SELECT id,address FROM `values` WHERE address LIKE :address AND name LIKE :name');
		$stmt->bindValue(':name', $name, SQLITE3_TEXT);
		$stmt->bindValue(':address', $address, SQLITE3_TEXT);
		$result = $stmt->execute();
		
		$return = array();
		while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
			$return[] = $row;
		}
		return (count($return) > 0);
	}
	
	public static function getAllWriteableValues(){
		global $db;
		$stmt = $db->prepare('SELECT id,object FROM `values` ');
		$result = $stmt->execute();
		
		$return = array();
		while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
			$value = Value::dec($row["object"]);
			if($value->isWriteable()){
				$return[$row["id"]] = "(".$value->getValueType().") ".$value->getName();
				
			}
		}
		return $return;
	}
	
	public function getValue(){
		$value = $this->datasource->getValueFromAddress($this->address);
		if($this->valueType == "integer" && floor($value) != $value){ //If integer and has decimals multiply by 100. This is a bit hacky
			$value = $value*100;
		}
		return $value;
	}
	
	/**
	 * @return string Returns a formatted string if the value is a double and in any other case the raw value. 
	 */
    public function getFormattedValue($getValue=false){

        if ($getValue == false){
            $getValue = $this->getValue();
        }

        if($this->valueType == "double") {
            return sprintf("%." . $this->configData["number_of_decimals"] . "f %s", $getValue, $this->configData["unit"]);
        }elseif($this->valueType == "integer"){
            return $this->getNameForValue($getValue);
        }else{
            return $getValue;
        }
    }
	
	public function setColors($background,$text){
		$this->colors["background-color"] = $background;
		$this->colors["color"] = $text;
		$this->storeToDatabase();
	}
	
	public function getColors(){
		return $this->colors;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function setValueType($type){
		$this->valueType = $type;
	}
	
	public function getValueType(){
		return $this->valueType;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function getDatasource(){
		return $this->datasource;
	}
	
	public function isWriteable(){
		return $this->isWriteable;
	}
	
	public function setWriteable($param){
		if(is_string($param)){
			//Is a string, do string comparisons
			if($param == "true"){
				$param = true;
			}else{
				$param = false;
			}
		}elseif(is_int($param)){
			if($param<=0){
				$param = false;
			}else{
				$param = true;
			}
		}
		$this->isWriteable = $param;
	}

	public function setValue($val){
		//Check if value can even be written
		if($this->isWriteable){
			return $this->datasource->writeValueToAddress($this->address,$val);
		}else{
			return false;
		}
	}
	
	public function setName($name){
		//Check if name is valid (not a duplicate, not null, ...)
		$this->name = $name;
	}
	
	public function getConfigData(){
		return $this->configData;
	}
	
	public function setConfigData($data){
		if(is_array($data)){
			foreach($data as $key=>$value){
				$this->configData[$key] = $value;
			}
		}
		$this->storeToDatabase();
	}
	
	public function getAllValueNames(){
		return $this->valueNames;
	}
	
	public function resetValueNames(){
		$this->valueNames = array();
	}
	
	public function setValueName($id,$name){
		$this->valueNames[$id] = $name;
		$this->storeToDatabase();
	}
	
	public function getNameForValue($id){
		return (isset($this->valueNames[$id]))?$this->valueNames[$id]:$id;
	}
	
	
	private function recognizeValueType(){
		$value = $this->getValue();
		if(is_numeric($value)){
			$value = $value*1; //Multiply by 1 to make numeric. 
		}
		switch (gettype($value)){
			case "double":
				$this->valueType = "double";
				break;
			case "integer":
				$this->valueType = "integer";
				break;
			case "boolean":
				$this->valueType = "boolean";
				break;
			case "string":
				$this->valueType = "string";
				break;
			case "NULL":
				$this->valueType = "NULL";
				break;
			default:
				debug("uncaught type:");
				debug(gettype($value));
		}
	}
	
	/**
	 * Stores the current object configuration in the database either updating or creating the entry
	 */
	public function storeToDatabase(){
		if($this->isTemp) return true;
		$this->configData["address"] = $this->address;
		$this->configData["isWriteable"] = $this->isWriteable;
		if($this->valueType == "NULL" || $this->valueType == "string" ){
			$this->recognizeValueType();
		}
		
		global $db;
		//Check if an ID is set
		if($this->id == null){
			//Check if the address exists already
			if($this->getIdFromAddress($this->address) === false){
				//Doesn't exist yet, insert
				$stmt = $db->prepare('INSERT INTO `values` (`name`,`object`,`configdata`,`address`,`objectType`) VALUES (:name,:object,:configdata,:address,:objectType)');
				$stmt->bindValue(':name', $this->name, SQLITE3_TEXT);
				$stmt->bindValue(':object', $this->enc($this), SQLITE3_TEXT);
				$stmt->bindValue(':configdata', $this->enc($this->getConfigData()), SQLITE3_TEXT);
				$stmt->bindValue(':address', $this->address, SQLITE3_TEXT);
				$stmt->bindValue(':objectType', get_class($this->datasource), SQLITE3_TEXT);
				$stmt->bindValue(':storeInRRD',$this->configData["log_in_rrd"]);
				
				$stmt->execute();
				$this->id = $this->getIdFromAddress($this->address);
				//Since the ID is now known update the entry again so the correct object is set
			}
		}
		//Update object in database
		$stmt = $db->prepare('UPDATE `values` SET `name`=:name,`object`=:object,`configdata`=:configdata,`address`=:address, `objectType`=:objectType, `store_in_rrd`=:storeInRRD WHERE `id`=:id');
		$stmt->bindValue(':name', $this->name, SQLITE3_TEXT);
		$stmt->bindValue(':object', $this->enc($this), SQLITE3_TEXT);
		$stmt->bindValue(':configdata', $this->enc($this->getConfigData()), SQLITE3_TEXT);
		$stmt->bindValue(':address', $this->address, SQLITE3_TEXT);
		$stmt->bindValue(':objectType', get_class($this->datasource), SQLITE3_TEXT);
		$stmt->bindValue(':storeInRRD',$this->configData["log_in_rrd"]);
		$stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
		
		$stmt->execute();
		$this->fromDatabase($this->id);
	}
	

	

	
	

}
