<?php

/**
 * One Wire Interfacing Class
 * 
 * 
 */

require_once "DataSource.php";
require_once "ownet.php";
require_once "OneWireValueHandler.php";

/** @var array Global Variable that will contain the OWnet objects for each opened connection so there are no duplicates. */
$OW_GLOBAL_CONNECTIONS = array();

/**
 * Class OneWire
 */
class OneWire implements DataSource
{
	/** 
	 * @var OWNet $owConn Private variable which is a reference to the OWNet object handling the connection to the One Wire Bus 
	 */
	private $owConn;
	function __construct($addr = "tcp://localhost")
	{
		global $OW_GLOBAL_CONNECTIONS;
		//First check if there is already a OWnet connection to this OWserver
		$serveraddr = str_replace("tcp://","",$addr);
		if(!isset($OW_GLOBAL_CONNECTIONS[$serveraddr])){
			
			$OW_GLOBAL_CONNECTIONS[$serveraddr] = new \OWNet("tcp://".$serveraddr);
		}
		$this->owConn = &$OW_GLOBAL_CONNECTIONS[$serveraddr];
	}

	/**
	 * Retrieves all available devices while omitting a bunch of debugging devices
	 * @return array
	 */
	function getAvailableDevices(){
		$sensorsToSkip = array("/bus.0","/bus.1","/simultaneous","/settings","/statistics","/structure","/system","/uncached");
		$dir = $this->owConn->dir("/");
		$return = array();
		foreach($this->dirToArray($dir) as $sensor) {
			if (in_array($sensor, $sensorsToSkip)) continue;
			$return[] = $sensor;
		}
		sort($return);
		return $return;
	}
	
	/**
	 * Retrieves the subparameters of an address
	 * @param string $address Address
	 * @return array|null|trim
	 */
	function getParameters($address){
		if($this->parameterExists($address)){
			return $this->dirToArray($this->owConn->dir($address));
		}else{
			return NULL;
		}
	}
	
	/**
	 * Checks if a parameter exists at a given address
	 * @param string $address Address
	 * @return bool
	 */
	function parameterExists($address){
		return ($this->owConn->dir($address) != NULL);
	}
	
	/**
	 * @param string $address A 1Wire address 
	 * @param array $valuesByType Reference to the array that contains the already stored parameters
	 * @return mixed Returns an array if there are subdevices of this device, otherwise returns the full address
	 */
	function getAllParametersOfDevice($address, &$valuesByType=array()){
		$return = array();
		$parameters = $this->getParameters($address);
		if($parameters == NULL){
			return $address;
		}else{
			$tmp = explode("/",$parameters[0]);
			$directory = $tmp[count($tmp)-2];
			$tmpArray = array();
			foreach($parameters as $parameter){
				$tmp2 = $this->getAllParametersOfDevice($parameter,$valuesByType);
				if(is_array($tmp2)){
					$tmp3 = array_keys($tmp2)[0];
					$tmpArray[$tmp3] = $tmp2[$tmp3];
				}else{
					$tmp3 = explode("/",$tmp2);
					$name = $tmp3[count($tmp3)-1];
					if(isset($valuesByType[$tmp2])){
						$tmpArray[$name] = $valuesByType[$tmp2];
						$tmpArray[$name]["parameter"] = $name;
						
					}else{
						$tmpArray[$name] = array("address"=>$tmp2,"parameter"=>$name);
					}
				}
			}
			$return[$directory] = $tmpArray;
		}
		return $return;
	}
	
	/**
	 * Checks the presence of an Address
	 * @param string $address Address
	 * @return array|null|string
	 */
	function exists($address){
		return $this->owConn->presence($address);
	}
	
	/**
	 * Retieves the value from an address
	 * @param string $address Address
	 * @return array|null|string
	 */
    function getValueFromAddress($address)
    {
        if(USE_1WIRE_VALUE_HANDLER)
        {
            return (new OneWireValueHandler($address))->getValueFromFile();
        }
        if(GET_VALUE_FROM_OWFS)
        {
            return ($data = file_get_contents(SET_OWFS_DIRECTORY_ADDRESS . $address) !== (bool) false) ? $data : NAN;
        }
        if(NUMBER_OF_RETRY_ON_ERROR)
        {
            return (new OneWireValueHandler($address))->getValueFromOwnet();
        }

        return $this->owConn->read($address)['data_php'];

    }
	
	/**
	 * Tries to write a value to an Address
	 * @param string $address Address
	 * @param mixed $value Value
	 * @return array|bool
	 */
	function writeValueToAddress($address,$value){
		return $this->owConn->set($address,$value);
	}
	
	/**
	 * Converts a directorystring to an array containing the tree
	 * @param string $dir String of a directory
	 * @return array
	 */
	private function dirToArray($dir){
		if(isset($dir["data_php"])){
			return array_map("trim",explode(",",$dir["data_php"]));
		}else{
			return array();
		}
	}
}