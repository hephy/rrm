<?php

/**
 * Created by PhpStorm.
 * User: Ruben Nati
 * Date: 07.03.17
 * Time: 01:10
 */

/**
 * Class FormulaExtended
 */

class FormulaExtended
{
    private $pos;
    private $requiredValues;
    private $formulaExtendedArray;

    function splitFormulaExtended($formula)
    {
        $this->pos = [strpos($formula, '['), strpos($formula, '>'), strpos($formula, ';'), strpos($formula, ';', strpos($formula, ';')+1), strpos($formula, ']')];
        $this->formulaExtendedArray = [substr($formula, $this->pos[0]+1, $this->pos[1]-$this->pos[4]-1), substr($formula, $this->pos[1]+1, $this->pos[2]-$this->pos[4]-1), substr($formula, $this->pos[2]+1, $this->pos[3]-$this->pos[4]-1), substr($formula, $this->pos[3]+1, -1)];

        preg_match_all("/(?<={)(.*?)(?=})/",$formula,$this->requiredValues);
        $this->requiredValues = array_unique($this->requiredValues[0]); //Remove duplicates and only store the first level of the array
        foreach($this->requiredValues as $valueId){
            $this->formulaExtendedArray[0] = str_replace("{".$valueId."}","v".$valueId,$this->formulaExtendedArray[0]);
            $this->formulaExtendedArray[1] = str_replace("{".$valueId."}","v".$valueId,$this->formulaExtendedArray[1]);
            $this->formulaExtendedArray[2] = str_replace("{".$valueId."}","v".$valueId,$this->formulaExtendedArray[2]);
            $this->formulaExtendedArray[3] = str_replace("{".$valueId."}","v".$valueId,$this->formulaExtendedArray[3]);
        }
    }

    function value($formula){

        $this->splitFormulaExtended($formula);

        $m = new EvalMath();
        foreach($this->requiredValues as $valueId){
            $value = Value::fromDatabase($valueId);
            $m->e("v".$valueId." = ".$value->getValue());  //Set variables for the EvalMath class
        }

        $valueOnLeft = $m->e($this->formulaExtendedArray[0]);
        $valueOnRight = $m->e($this->formulaExtendedArray[1]);

        if(is_numeric($this->formulaExtendedArray[2]) && is_numeric($this->formulaExtendedArray[3]) ) {
            if (!isset($this->formulaExtendedArray[2][1]) && !isset($this->formulaExtendedArray[3][1]) && (0 == $this->formulaExtendedArray[2] || 0 == $this->formulaExtendedArray[3])) {
                $valueFirst = boolval($m->e($this->formulaExtendedArray[2]));
                $valueSecond = boolval($m->e($this->formulaExtendedArray[3]));
            } else {
                $valueFirst = floatval($m->e($this->formulaExtendedArray[2]));
                $valueSecond = floatval($m->e($this->formulaExtendedArray[3]));
            }
        }else{
            $valueFirst = $m->e($this->formulaExtendedArray[2]);
            $valueSecond = $m->e($this->formulaExtendedArray[3]);
        }

        if (strcmp($this->formulaExtendedArray[2], '$()') == 0 )
        {
            return ($valueOnLeft > $valueOnRight) ? floatval($valueOnLeft) : $valueSecond;
        }

        if (strcmp($this->formulaExtendedArray[3], '$()') == 0 )
        {
            return ($valueOnLeft > $valueOnRight) ? $valueFirst : floatval($valueOnLeft);
        }

        return ($valueOnLeft > $valueOnRight) ? $valueFirst : $valueSecond;
    }

    function checkFormulaExtended($formula){

        preg_match_all("/(?<={)(.*?)(?=})/",$formula,$requiredValues);
        $requiredValues = array_unique($requiredValues[0]); //Remove duplicates and only store the first level of the array
        $convertedFormula = $formula;
        foreach($requiredValues as $valueId){
            $convertedFormula = str_replace("{".$valueId."}","v".$valueId,$convertedFormula);
        }
        //Now that we have the converted Formula check if all the values used really are double values that can be used in formulas
        $m = new EvalMath();
        foreach($requiredValues as $valueId){
            $value = Value::fromDatabase($valueId);
            if($value->getValueType() != "double"){
                return array("error"=>true,"desc"=>"Value '".$valueId."' is no double");
            }
            $m->e("v".$valueId." = ".$value->getValue());  //Set variables for the EvalMath class
        }

        return array("error"=>false,"desc"=>"No Error found"); //Evaluation returned a value, everything seems to be ok

    }
}