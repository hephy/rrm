<?php

/**
 * Main DataSource Interface
 * 
 * This interface defines the basis of every datasource 
 */


/**
 * Interface DataSource
 */
interface DataSource
{
	/**
	 * Retrieve a value from a given Address
	 * @param $address
	 * @return mixed Value from that address
	 */
	function getValueFromAddress($address);
	
	/**
	 * Write a value to a given Address
	 * @param string $address Address to be written to
	 * @param mixed $val Value to be written to this address
	 * @return bool Success of the write operation
	 */
	function writeValueToAddress($address,$val);	
}
