<?php

/**
 * Created by PhpStorm.
 * User: Ruben Nati
 * Date: 13.12.16
 * Time: 20:32
 */

/**
 * Class OneWireValueHandler
 */

class OneWireValueHandler
{
    private $owNetLocal;
    private $valueAddress;
    private $path1wireRAMDisk = '/dev/shm/1wireRAMDisk';
    private $timeInterval = 5;
    private $dataFromFile;

    function __construct($address)
    {
        $this->owNetLocal = new OWNet();
        $this->valueAddress = $address;
    }

    public function getValueFromFile()
    {
        if (!file_exists($this->path1wireRAMDisk . $this->valueAddress))
        {
            $this->createFile();
        }

        while (false == ($this->dataFromFile = file($this->path1wireRAMDisk . $this->valueAddress))) {
        }

        if(false != $this->dataFromFile && isset($this->dataFromFile[1]))
        {
            if ($this->timeInterval > (microtime(true) - $this->dataFromFile[0]))
            {
                return $this->returnTheRightFormatFromFile(trim($this->dataFromFile[1]));
            }

            return $this->returnTheRightFormatFromFile(trim($this->writeValueToFile()));
        }

        return NAN;
    }

    public function writeValueToFile()
    {
        if(flock($file = fopen($this->path1wireRAMDisk . $this->valueAddress, 'w+'), LOCK_EX | LOCK_NB))
        {
            fwrite($file, implode("", array(microtime(true) . "\n", ($value = $this->getValueFromOwnet()) . "\n")));
            fflush($file);
            flock($file, LOCK_UN);
            fclose($file);
            return $value;
        }
        else
        {
            return $this->dataFromFile[1];
        }
    }

    public function createFile()
    {
        shell_exec("mkdir -p " . $this->path1wireRAMDisk . substr($this->valueAddress, 0, strrpos( $this->valueAddress, '/')));

        if(flock($file = fopen($this->path1wireRAMDisk . $this->valueAddress, 'w+'), LOCK_EX | LOCK_NB)) {
            fwrite($file, implode("", array(microtime(true) . "\n", ($value = $this->getValueFromOwnet()) . "\n")));
            fflush($file);
            flock($file, LOCK_UN);
            fclose($file);
        }
    }

    public function writeRightFormatToFile($data)
    {
        if('1' === $data)
        {
            return 'true';
        }
        elseif('0' === $data)
        {
            return 'false';
        }
        else
        {
            return trim($data);
        }
    }

    public function returnTheRightFormatFromFile($data)
    {
        if(is_numeric($data))
        {
            return (strpos($data,".") == false) ? intval($data) : floatval($data);
        }
        elseif(strncmp($data, 'true', 4) == 0)
        {
            return (bool) true;
        }
        elseif(strncmp($data, 'false', 4) == 0)
        {
            return (bool) false;
        }
        elseif(strncmp($data, 'NAN', 3) == 0)
        {
            return NAN;
        }
        else
        {
            return $data;
        }
    }

    public function getValueFromOwnet()
    {
        if(NUMBER_OF_RETRY_ON_ERROR)
        {
            return (USE_1WIRE_VALUE_HANDLER) ? $this->writeRightFormatToFile($this->getValueFromOwnetAndRetryOnError()) : $this->returnTheRightFormatFromFile($this->writeRightFormatToFile($this->getValueFromOwnetAndRetryOnError()));
        }
        else
        {
            return $this->writeRightFormatToFile($this->owNetLocal->read($this->valueAddress)['data']);
        }
    }

    public function getValueFromOwnetAndRetryOnError($counter = 0)
    {
        if (NULL == ($data = $this->owNetLocal->read($this->valueAddress)) || (1 > $data['data_len']))
        {
            do
            {
                $counter++;
                $data = $this->owNetLocal->read($this->valueAddress);
            }
            while(($counter < NUMBER_OF_RETRY_ON_ERROR) && ((NULL == $data) || (1 > $data['data_len'])));

            if(ERROR_LOGGER_SET && ($counter > 1))
            {
                $GLOBALS['defaultLogger']->log("OWNet tcp connection failed (tries:" . $counter . ", addr:" . $this->valueAddress. ")", 2);
            }

            return (isset($data['data_len']) && (0 < $data['data_len'])) ? $data['data'] : NAN;
        }
        else
        {
            return $data['data'];
        }
    }
}