<?php

/**
 * EIB/KNX interfacing class
 */

require_once "DataSource.php";
#require_once "knx/eibclient.php";

/**
 * Class KNX
 */
class KNX implements DataSource
{
	private $conn = "192.168.0.5";
	
	/**
	 * KNX constructor. Mainly sets the IP of the KNXD server
	 * @param string $ip
	 */
	function __construct($ip="192.168.0.5")
	{
		$this->conn = $ip;
	}
	
	/**
	 * Retrieves the value from the datasource and converts it as well. The implementation is a bit hacky sine a process is forked and the output read out. The error output is redirected to /dev/null to not have it completely fill up the error log. 
	 * @param string $address
	 * @return float
	 */
	function getValueFromAddress($address){
		//Performing this task in the inefficient way because I can't get the library to work
		$fp = popen("/usr/lib/knxd/groupreadresponse ip:".$this->conn." ".$address." 2> /dev/null","r");
		$response = fread($fp,2024);
		preg_match("/:.*/",$response,$responseValue);
		$responseValue = trim(str_replace(":","",$responseValue[0]));
		//Identify if the Value is 1,8 or 16 bit long and format it accordingly. 
		if(strlen($responseValue)==5){
			return $this->hexTo16Float($responseValue);
		}else{
			return $this->hexTo16Float($responseValue)*100;
		}
		
	}
	
	/**
	 * Writes a value to a given address, also quite hacky
	 * @param string $address
	 * @param mixed $value
	 * @return bool
	 */
	function writeValueToAddress($address,$value){
		if($value>1){
			$value = $this->floatTo16Hex($value);
		}
		//Performing this task in the inefficient way because I can't get the library to work
		$fp = popen("/usr/lib/knxd/groupwrite ip:".$this->conn." ".$address." ".$value,"r");
		$response = fread($fp,2024);  //Just read it
//		debug("/usr/lib/knxd/groupwrite ip:192.168.0.5 ".$address." ".$value);
		sleep(1);  //Wait for the writing to have set
		//Check if set value is now equal to the value that was supposed to be set
		return ($value == $this->floatTo16Hex($this->getValueFromAddress($address)));
	}
	
	/**
	 * Converts a 16 bit hex value to a float value with the value being divided by 100 for decimals
	 * @param $strHex string A Hex String in a format like 0C 01
	 * @return float
	 */
	public static function hexTo16Float($strHex) {
		$v = hexdec($strHex);
		$x = ($v & ((1 << 11) - 1)) *($v >> 15 | 1);
		$exp = ($v >> 11);
		$tmp = 0.01*$x * pow(2, $exp);
		return ($tmp);
	}
	
	/**
	 * Converts a float Value to a hex value using 16 bit precision and multiplies the value by 100 to remove decimals
	 * @param $float float The float value to be converted
	 * @return string A Hex String in the format 0C 01
	 */
	public static function floatTo16Hex($float) {
		$mantisse = $float*100/2;
		$exp = 1;
		$sign = ($float<0)?1:0;
		$hex = dechex(($sign << 15)+($exp << 11)+$mantisse);
		if(strlen($hex)==3){
			return "0".substr($hex,0,1)." ".substr($hex,1,2);
		}elseif(strlen($hex)==4){
			return substr($hex,0,2)." ".substr($hex,2,2);
		}
	}
	
}
