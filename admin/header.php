<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Admin - Cleanroom Monitor</title>
	<script type="text/javascript" src="../lib/jquery-2.2.2.js"></script>
	<!--<script type="text/javascript" src="../lib/gridster/jquery.gridster.with-extras.js"></script>-->
	<script type="text/javascript" src="../lib/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript" src="../lib/jquery-ui/jquery-collision.js"></script>
	<script type="text/javascript" src="../lib/jquery-ui/jquery-ui-draggable-collision.js"></script>
	<link rel="stylesheet" href="../lib/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="../lib/jquery-ui/jquery-ui.structure.css" />
	<link rel="stylesheet" href="../lib/jquery-ui/jquery-ui.theme.css" />
<?php
require_once "../conf/conf.php";
