<?php

/**
 * User: Benedikt Würkner
 */

/**
 * The class describing a "Group" object. An element used in the layout configurator to group multiple elements together
 * Class Group
 */
class Group
{
	/**
	 * @var string $style Style definition
	 */
	private $style;
	/**
	 * @var string $name Name of Group
	 */
	private $name;
	/**
	 * @var int $groupId Id of Group
	 */
	private $groupId;
	/**
	 * @var array $elements All the Elements within this group
	 */
	private $elements=array();
	/**
	 * @var int $defaultLayoutWidth The default width of the layout (Required for the calculation of the group width)
	 */
	protected $defaultLayoutWidth = 1257;
	/**
	 * @var int $defaultLayoutHeight The default width of the layout (Required for the calculation of the group height)
	 */
	protected $defaultLayoutHeight = 703;
	
	/**
	 * Group constructor.
	 * @param string $name The Name of this Group
	 * @param string $style The style options (if existing)
	 */
	function __construct($name,$style="")
	{
		$this->name = $name;
		$this->style = $style;
	}
	
	/**
	 * Defines the Style of the group
	 * @param string $style The style options
	 */
	function setStyle($style){
		$this->style = $style;
	}
	
	/**
	 * Setter for Group ID
	 * @param int $id The id of the group
	 */
	function setGroupId($id){
		$this->groupId = $id;
	}
	
	/**
	 * Adds an Element to this group
	 * @param Element $element The element to be added
	 */
	function addElement(Element $element){
		$this->elements[] = $element;
	}
	
	/**
	 * Generates the edit view of this group and all contained elements
	 * @return string
	 */
	public function getEditView(){
		$return = "<div class='group obstacle' id='group_".$this->groupId."' style='".$this->style."'>";
		$return .= "<div class='name'>".$this->name."</div>";
		//Insert Elements
		foreach ($this->elements as $element){
			$return .= $element->getEditView();
		}
		$return .=  "</div>";
		return $return;
	}
	
	/**
	 * Generates the view of this Group and all contained Elements
	 * @return string
	 */
	public function getDisplayView(){
		$tmp = explode(";",$this->style);
		$css = array();
		foreach($tmp as $setting){
			$tmp2 = array_map("trim",explode(":",$setting));
			if($tmp2[0] != "") $css[$tmp2[0]] = $tmp2[1];
		}
		$width  = $css["width"];
		$height = $css["height"];
		//Convert pixel dimensions to percentages for viewing
		$css["width"] = (str_replace("px","",$css["width"])*100/$this->defaultLayoutWidth)."%";
		$css["left"] = (str_replace("px","",$css["left"])*100/$this->defaultLayoutWidth)."%";
		$css["top"] = (str_replace("px","",$css["top"])*100/$this->defaultLayoutHeight)."%";
		$css["height"] = (str_replace("px","",$css["height"])*100/$this->defaultLayoutHeight)."%";

		$style = "";
		foreach($css as $key=>$value){
			$style .= $key.":".$value.";";
		}
		$return = '<div class="group" style="'.$style.'">';
		$return .= "<div class='name'>".$this->name."</div>";
		foreach($this->elements as $element){
			$return .= $element->getDisplayView($width,$height);
		}
		$return .= '</div>';
		return $return;
	}
	
	/**
	 * Encodes this object as base64
	 * @return string a base64 string encoding the original variable
	 */
	public function enc(){
		return base64_encode(serialize($this));
	}
	
}
