<?php
/**
 * Created by PhpStorm.
 * Simple Test File that can be used to test stuff during developement 
 * 
 * Date: 06/04/16
 * Time: 17:10
 */

error_reporting(E_ALL);
ini_set('display_errors', true);
require "header.php";

require "../lib/KNX.php";

//KNS Klimaanlage Sensor Reinraum: 192.168.0.238

require_once "../lib/knx/eibnetmux.php";

#$conn = new eibnetmux("Klimaanlage","192.168.0.5",6720);
$conn = new EIBConnection("192.168.0.5");
$buffer = new EIBBuffer("");

function gaddrparse ($addr)
{
	$addr = explode ("/", $addr);
	if (count ($addr) >= 3)
		$r =
			(($addr[0] & 0x1f) << 11) | (($addr[1] & 0x7) << 8) |
			(($addr[2] & 0xff));
	if (count ($addr) == 2)
		$r = (($addr[0] & 0x1f) << 11) | (($addr[1] & 0x7ff));
	if (count ($addr) == 1)
		$r = (($addr[1] & 0xffff));
	return $r;
}
function groupsresponse(EIBConnection $con, $addr, $val) {
	$addr = gaddrparse ($addr);
	$val = ($val + 0) & 0x3f;
	$val |= 0x0040;
	$r = $con->EIBOpenT_Group ($addr, 0);
	debug("EIBOpenT_Group ".$r);
	if ($r == -1) return -1;
	$r = $con->EIBSendAPDU (pack ("n", $val));
	debug("EIBSendAPDU ".$r);
	if ($r == -1) return -1;
	return $con->EIBReset ();
}

$addr = "0/0/30";
$src = new EIBAddr($addr);

$conn->EIB_Cache_Enable();
debug("GroupsResponse ".groupsresponse($conn,$addr,3));
debug($conn->EIB_Cache_Read(gaddrparse($addr),$src,$buffer));
for($i=0;$i<2;$i++){
	$len = $conn->EIB_Poll_Complete();
	if($len == 0){
		debug($buffer);
		debug("EIBGetAPDU ".$conn->EIBGetAPDU_async($buffer));
		debug($buffer);
	}
}


//Can either be used by using popen("/usr/lib/knxd/groupwrite ip:127.0.0.1 0/0/x OC B0"); or maybe via a php library TODO!
$fp = popen("/usr/lib/knxd/groupreadresponse ip:192.168.0.5 0/0/11","r");
debug(fread($fp,2024));
require "footer.php";
exit();


$test = new EIBConnection("192.168.0.5");
$src = new EIBAddr();
$src->addr = "0/0/11";

$dest = new EIBAddr("0/0/11");


$data = "11";

$test->EIB_M_ReadIndividualAddresses($buffer);

debug($test->EIBOpenT_Group(15,false));
debug($test->EIBSendGroup("0/0/11",$data));
debug($test);

$gpio = new GpioPHP();
//
//

//
//
//#$CA1->getIdFromAddress("/26.90AF40010000/temperature");
//#$CA1->getIdFromAddress(17);
//debug($CA1->getValue());






// ------- GPIO -------

require "../lib/php-gpio/vendor/autoload.php";
use PhpGpio\Gpio;
$gpio = new GPIO();
//Get input from GPIO Pin No 17
var_dump($gpio->input(17));

// ------- KNX -------

//Install knxd first from https://github.com/knxd/knxd

//Can either be used by using popen("/usr/lib/knxd/groupwrite ip:127.0.0.1 0/0/x OC B0"); or maybe via a php library TODO!

require("../lib/knx/eibclient.php");
//KNS Klimaanlage Sensor Reinraum: 192.168.0.238


$test = new EIBConnection("localhost");

// ------- 1WIRE -------

//Requires this small library that handles the socket/stream connection to the owserver

$Temp1 = Value::create("Temp 1",$oneWire,"26.90AF40010000/temperature");

$devices = $oneWire->getAvailableDevices();

foreach($devices as $device){
	debug($oneWire->getSubdevices($device));
}

/**
 * Converts a 16 bit hex value to a float value with the value being divided by 100 for decimals
 * @param $strHex string A Hex String in a format like 0C 01
 * @return float
 */
function hexTo16Float($strHex) {
	$v = hexdec($strHex);
	$x = ($v & ((1 << 11) - 1)) *($v >> 15 | 1);
	$exp = ($v >> 11);
	return 0.01*$x * pow(2, $exp);
}

/**
 * Converts a float Value to a hex value using 16 bit precision and multiplies the value by 100 to remove decimals
 * @param $float float The float value to be converted
 * @return string A Hex String in the format 0C01
 */
function floatTo16Hex($float) {
	$mantisse = $float*100/2;
	$exp = 1;
	$sign = ($float<0)?1:0;
	$hex = dechex(($sign << 15)+($exp << 11)+$mantisse);
	return (strlen($hex)==3)?"0".$hex:$hex;
}
