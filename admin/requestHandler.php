<?php
/**
 * File that handles all the requests sent from the admin interface and returns the expected values as JSON or HTML
 * 
 * Date: 18/04/16
 */

$r = (isset($_POST["r"]))? $_POST["r"]:"error";

if(isset($_GET["r"])){  //In some rare cases also accept GET requests
	$r = ($_GET["r"] == "getRawValueById" || $_GET["r"] == "checkLimits" || $_GET["r"] == "renderLayout" || $_GET["r"] == "getAllValuesForRRD")?$_GET["r"]:$r;
}
require "../conf/conf.php";
require "Element.php";
require "Group.php";
require "Limit.php";
require "../lib/DataSources/LocalValue.php";
require "../lib/DataSources/Formula.php";

switch($r) {
	case "checkLimits":
		$valueIds = Limit::getAllLimits();
		$valueIds = array_unique($valueIds);
		foreach($valueIds as $valueId){
			$limits = Limit::getLimitsForValueId($valueId);
			foreach($limits as $limit){
				$limit->checkLimits();
			}
		}
		echo runtime("Checking limits",true);
		break;
	
	case "removeLimit":
		if(isset($_POST["limit_id"])){
			$limit = Limit::getLimitById($_POST["limit_id"],0);
			$limit->delete();
			echo json_encode(array("error"=>false));
		}
		break;
	
	case "getValueById":
		if(isset($_POST["id"])) {
			echo getValueById($_POST["id"]);
		}elseif(isset($_GET["id"])){
			echo getValueById($_GET["id"]);
		}else{
			parameterError();
		}
		break;
	
	case "getRawValueById":
		if(isset($_POST["id"])) {
			echo getValueById($_POST["id"],true);
		}elseif(isset($_GET["id"])){
			echo getValueById($_GET["id"],true);
		}else{
			parameterError();
		}
		break;
	
	case "getValueByAddress":
		if(isset($_POST["connType"]) && isset($_POST["address"])){
			echo getValueByAddress($_POST["connType"],$_POST["address"]);
		}else{
			parameterError();
		}
		break;
	
	case "getLog":
		if(isset($_POST["loglevel"])){
			$rows = (isset($_POST["rows"]))?$_POST["rows"]:30;
			echo json_encode($defaultLogger->getLog($_POST["loglevel"],$rows));
		}else{
			parameterError();
		}
		break;
	
	case "getAllValuesForRRD": //Returns a json array of all the value ids and their current values for storage in the RRD.
		$stmt = $db->prepare('SELECT id FROM `values` WHERE store_in_rrd = 1');
		$result = $stmt->execute();
		while ($group = $result->fetchArray(SQLITE3_ASSOC)) {
			$ids[] = $group["id"];
		}
		$return = array(); 
		foreach($ids as $id){
			$return["values"][$id] = json_decode(getValueById($id));
		}
		$return["databases"] = $rrdDatabases;
		$return["scripts"] = $rrdScripts;
		echo json_encode($return);
		break;
	
	case "renderLayout":
		if(isset($_GET["layout_id"])){
			echo renderLayout($isNew = False, $_GET["layout_id"]);
		}else{
			parameterError();
		}
		break;
	
	case "previewGraph":
		require "GraphConfiguration.php";
		$graphC = new GraphConfiguration();
		$tmp = $graphC->checkConfiguration($_POST["id"],-21600,$_POST["title"],$_POST["vertical"],$_POST["width"],$_POST["height"],$_POST["configuration"],true);
		echo json_encode($tmp);
		break;

	case "saveGraph":
		require "GraphConfiguration.php";
		$graphC = new GraphConfiguration();
		echo $graphC->saveConfiguration($_POST["id"],$_POST["name"],$_POST["title"],$_POST["vertical"],$_POST["width"],$_POST["height"],$_POST["configuration"]);
		//Generate .sh files
		$stmt = $db->prepare('SELECT * FROM `graphs`');
		$result = $stmt->execute();
		$graphs = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$graphs[$row["id"]] = $row;
		}
		foreach($graphsTimeframes as $name=>$startTime){
			$filename = "graphs_".$name.".sh";
			$data = "#! /bin/sh \n";
			foreach($graphs as $graph){
				$graph["timeSuffix"] = $name;
				$data .= $graphC->createConfiguration($graph,$startTime*-1)."\n";
			}
			file_put_contents($rrdScripts.$filename,$data);
		}
		break;

    case "deleteGraph":
        require "GraphConfiguration.php";
        $graphC = new GraphConfiguration();
        echo $graphC->deleteConfiguration($_POST["id"]);

        //Generate .sh files
        $stmt = $db->prepare('SELECT * FROM `graphs`');
        $result = $stmt->execute();
        $graphs = array();
        while($row = $result->fetchArray(SQLITE3_ASSOC)){
            $graphs[$row["id"]] = $row;
        }
        foreach($graphsTimeframes as $name=>$startTime) {
            $filename = "graphs_" . $name . ".sh";
            $data = "#! /bin/sh \n";
            foreach ($graphs as $graph) {
                $graph["timeSuffix"] = $name;
                $data .= $graphC->createConfiguration($graph, $startTime * -1) . "\n";
            }
            file_put_contents($rrdScripts . $filename, $data);
        }
        break;
	
	case "nameIsUsed":
		if(isset($_POST["name"]) && isset($_POST["address"])){
			//Check if name is already set for a different address
			if(Value::nameIsSet($_POST["name"])){
				//Name is set, check if it is set for the same address
				if(Value::existsInDatabase($_POST["name"],$_POST["address"])){
					//Name and address are already set like this allow update (although not leading to anything)
					echo json_encode(array("isUsed"=>false));
				}else{
					//Name is already used but for a different address, prevent usage
					echo json_encode(array("isUsed"=>true));
				}
			}else{
				//Name is not set, allow usage
				echo json_encode(array("isUsed"=>false));
			}
		}else{
			parameterError();
		}
		break;
		
	
	case "deleteValueById":
		if(isset($_POST["id"])) {
			$valueId = $_POST["id"];
		}elseif(isset($_GET["id"])){
			$valueId = $_GET["id"];
		}else{
			parameterError();
			break;
		}
		$stmt = $db->prepare("DELETE FROM `values` WHERE id=:id");
		$stmt->bindParam(":id",$valueId);
		$stmt->execute();
		echo json_encode(array("error"=>false));
		break;
	
	case "assignIdToAddress":
		if(isset($_POST["name"]) && isset($_POST["address"]) && isset($_POST["datasource"])){
			//Check if ID is passed
			$isWriteable = (isset($_POST["isWriteable"]))? $_POST["isWriteable"]:false;
			$isWriteable = (isset($_POST["direction"]))?($_POST["direction"]=="out"):$isWriteable;  //Set to writeable if the pin is set to "output"
			
			if(isset($_POST["parameter_id"])){
				$value = Value::fromDatabase($_POST["parameter_id"]);
				$value->setAddress($_POST["address"]);
				$value->setName($_POST["name"]);
				if(isset($_POST["valueType"])) $value->setValueType($_POST["valueType"]);
				$value->setWriteable($isWriteable);
				$value->storeToDatabase();
				if($_POST["datasource"] == "GPIO"){
					if(!$gpio->setup($_POST["address"],$_POST["direction"])){
						echo json_encode(array("error"=>true,"desc"=>$gpio->getError()));
						break;
					}
					$gpio->saveConfiguration();
				}
				echo json_encode(array("id"=>$_POST["parameter_id"],"error"=>false,"isWriteable"=>$isWriteable));
			}else{
				//Check if address is already assigned
				if(Value::addressIsSet($_POST["address"])){ //should now be removable, leaving it in here for fallback and have to check the warnings at some point
					//Is already assigned, update name
					Value::updateName(Value::getIdFromAddress($_POST["address"]),$_POST["name"]);
					$defaultLogger->logWarning("Something used the address check: ".var_export($_POST,true));
					echo json_encode(array("error"=>false));
				}else{
					$valueType = "NULL";
					//Is not already assigned, create
					switch($_POST["datasource"]){
						case "OneWire":
							$datasource = &$oneWire;
							break;
						case "GPIO":
							$datasource = &$gpio;
							$valueType = (isset($_POST["valueType"]))?$_POST["valueType"]:$valueType;
							if(!$gpio->setup($_POST["address"],$_POST["direction"])){
								echo json_encode(array("error"=>true,"desc"=>$gpio->getError()));
								break(2);
							}
							$gpio->saveConfiguration();
							break;
						case "KNX":
							$datasource = &$knx;
							$valueType = $_POST["valueType"];
							break;
						case "LocalValue":
							$datasource = new LocalValue();
							$valueType = $_POST["valueType"];
							$isWriteable = true;
							break;
						default:
							$datasource = null;
					}
					
					$newvalue = Value::create($_POST["name"],$datasource,$_POST["address"],$isWriteable,$valueType);
					echo json_encode(array("id"=>$newvalue->getId(),"error"=>false));
				}
			}
		}else{
			parameterError();
		}
		break;

	case "saveLayout":
		if(isset($_POST["name"]) && isset($_POST["layout_id"])){
			//Update layout Name
			if(!is_numeric($_POST["layout_id"])){ //If the id is not yet numeric remove the "layout_" prefix 
				$_POST["layout_id"] = substr($_POST["layout_id"],7);
			}
			//Check if layout exists
			$stmt = $db->prepare('SELECT count(*) AS count FROM `layouts` WHERE id LIKE :layout_id');
			$stmt->bindValue(':layout_id', $_POST["layout_id"], SQLITE3_INTEGER);
			$result = $stmt->execute();
			$row = $result->fetchArray();
			$numRows = $row['count'];
			$newLayout = false; //Defines if a layout is created from a new one and can be used in the interface to act accordingly
			//prepare correct statement
			if($numRows>0){
				$stmt = $db->prepare('UPDATE `layouts` SET  `name`=:name WHERE `id`=:id');
			}else{
				$stmt = $db->prepare('INSERT INTO `layouts` (id,name) VALUES(:id,:name)');
				$newLayout = true;
			}
			$stmt->bindValue(':name', $_POST["name"], SQLITE3_TEXT);
			$stmt->bindValue(':id', $_POST["layout_id"], SQLITE3_INTEGER);
			$stmt->execute();

			$deletedGroups = array();
			$deletedElements = array();
			if(isset($_POST["deletedElements"])){
				foreach($_POST["deletedElements"] as $del){
					if(substr_compare($del,"group",0,5)==0){
						$deletedGroups[] = substr($del,6);
					}
					if(substr_compare($del,"eleme",0,5)==0){
						$deletedElements[] = substr($del,8);
					}
				}
			}

			foreach($deletedGroups as $groupID){
				//Remove all deleted groups (if any) and all attached elements
				$stmt = $db->prepare("DELETE FROM `groups` WHERE `id`=:id;");
				$stmt->bindValue(':id', $groupID, SQLITE3_INTEGER);
				$stmt->execute();
				
				$stmt = $db->prepare("DELETE FROM `elements` WHERE `group_id`=:id;");
				$stmt->bindValue(':id', $groupID, SQLITE3_INTEGER);
				$stmt->execute();
			}
			
			foreach($deletedElements as $elementID){
				//Remove all deleted elements (if any)
				$stmt = $db->prepare("DELETE FROM `elements` WHERE `id`=:id;");
				$stmt->bindValue(':id', $elementID, SQLITE3_INTEGER);
				$stmt->execute();
			}
			
			
			$_group_ID_matching = array();
			$_element_ID_matching = array();
			if(isset($_POST["groups"])){
				foreach($_POST["groups"] as $group){
					saveGroup($group,$_POST["layout_id"]);
				}
				//Groups passed, look for elements
				if(isset($_POST["elements"])){
					//Elements set
					foreach($_POST["elements"] as $element){
						saveElement($element);
					}
				}else{
					//No elements set
				}
			}else{
				//No groups passed, remove all groups from this layout since the parameter is empty if the array was empty
			}
			echo json_encode(array("groups"=>$_group_ID_matching,"elements"=>$_element_ID_matching,"newLayout"=>$newLayout));
		}
		break;

    case "deleteLayout":

        if(!is_numeric($_POST["layout_id"])){ //If the id is not yet numeric remove the "layout_" prefix
            $_POST["layout_id"] = substr($_POST["layout_id"],7);
        }

        $layout_id = $_POST["layout_id"];

        $stmt = $db->prepare('SELECT id FROM `groups` WHERE layout_id = :layout_id');
        $stmt->bindValue(':layout_id', $layout_id, SQLITE3_INTEGER);
        $result = $stmt->execute();
        echo 'layoutId:' . $layout_id . '<br>';

        while ($group_id = $result->fetchArray(SQLITE3_ASSOC)) {
            $stmt = $db->prepare('DELETE FROM `elements` WHERE group_id = :group_id');
            $stmt->bindValue(':group_id', $group_id["id"], SQLITE3_INTEGER);
            $stmt->execute();
            echo 'groupID:' . $group_id["id"] . '<br>';
        }

        $stmt = $db->prepare('DELETE FROM `groups` WHERE layout_id = :layout_id');
        $stmt->bindValue(':layout_id', $layout_id, SQLITE3_INTEGER);
        $stmt->execute();

        $stmt = $db->prepare('DELETE FROM `layouts` WHERE id = :layout_id');
        $stmt->bindValue(':layout_id', $layout_id, SQLITE3_INTEGER);
        $stmt->execute();

        break;

	case "getOverlayContent":
		if(isset($_POST["content"])){
			echo getContentForOverlay($_POST["content"]);
		}else{
			echo "Error - no content requested";
		}
		
		break;
	
	case "getLayout":
		if(isset($_POST["layout_id"])){
			$stmt = $db->prepare('SELECT id,name,position FROM `groups` WHERE layout_id LIKE :layout_id');
			$stmt->bindValue(':layout_id', $_POST["layout_id"], SQLITE3_INTEGER);
			$result = $stmt->execute();
			
			$groups = array();
			while ($group = $result->fetchArray(SQLITE3_ASSOC)) {
				$groups[$group["id"]] = new Group($group["name"],$group["position"]);
			}
			foreach($groups as $group_id=>$group){
				$stmt = $db->prepare('SELECT id,name, group_id,display_object FROM `elements` WHERE group_id LIKE :group_id');
				$stmt->bindValue(':group_id', $group_id, SQLITE3_INTEGER);
				$result = $stmt->execute();
				while ($element = $result->fetchArray(SQLITE3_ASSOC)) {
					$elem = Element::dec($element["display_object"]);
					$groups[$element["group_id"]]->addElement($elem);
				}
				
			}

			foreach($groups as $group){
				echo $group->getDisplayView();
			}
	
		}else{
			echo "No Layout ID Given!";
		}
		break;
	
	case "previewLayout":
		if(isset($_POST["groups"])){
			$groups = array();
			foreach($_POST["groups"] as $group){
				if(!is_numeric($group["id"])){ //If the id is not yet numeric remove the "group_" prefix 
					$group["id"] = substr($group["id"],6);
				}
				$groups[$group["id"]] = new Group($group["name"],$group["style"]);
				
			}
			//Groups passed, look for elements
			if(isset($_POST["elements"])){
				//Elements set
				foreach($_POST["elements"] as $element){
					if(!is_numeric($element["group"])){ //If the id is not yet numeric remove the "group_" prefix 
						$element["group"] = substr($element["group"],6);
					}
					if(!is_numeric($element["id"])){ //If the id is not yet numeric remove the "group_" prefix 
						$element["id"] = substr($element["id"],8);
					}
					$additionalParameters = array();
					if(isset($element["target"])){
						$additionalParameters["target"]=$element["target"];
					}
					if(isset($element["rows"])){
						$additionalParameters["rows"]=$element["rows"];
					}
					if(!isset($element["valueId"])){
						$element["valueId"] = 123456;
					}
					if(!isset($element["name"])){
						$elem = new Element($element["valueId"],$element["valueName"],$additionalParameters);
					}else{
						$elem = new Element($element["valueId"],$element["name"],$additionalParameters);
					}
					$elem->setStyle($element["style"]);
					$elem->setClass($element["class"]);
					$groups[$element["group"]]->addElement($elem); 
				}
			}else{
				//No elements set
			}
			foreach($groups as $group){
				echo $group->getDisplayView();
			}
		}
		break;
	
	case "saveValue":
		if(isset($_POST["value_id"])){
			$_POST["log_in_rrd"] = (isset($_POST["log_in_rrd"]))?1:0; //Set the correct value for this checkbox 
			$value = Value::fromDatabase($_POST["value_id"]);
			$value->setName($_POST["label"]);
			$value->setConfigData($_POST);
			echo json_encode(array(
				"value"=>$value->getValue(),
				"previewValue"=>$value->getFormattedValue()
			));
			
		}
		break;
	
	//Tries to set the value of a parameter to the given value 
	case "setValue":
		if(isset($_POST["value_id"]) && isset($_POST["valueValue"])){
			$value = Value::fromDatabase($_POST["value_id"]);
			$value->setWriteable(true);
			$value->setValue($_POST["valueValue"]);
			$value->storeToDatabase();
		}
		break;
	
	case "getFormulaValue":
		if(isset($_POST["formula"])){
			$check = Formula::checkFormula($_POST["formula"]);
			if($check["error"] == false){
				$f = new Formula($_POST["formula"]);
				if(isset($_POST["value_id"])){
					if($_POST["save"]=="true"){
						$value = Value::fromDatabase($_POST["value_id"]);
						$value->updateDatasource($f);
					}else{
						$value = Value::createTemp($_POST["label"], $f, ""); 
					}
					$id = ($value->getId() != null)?$value->getId():$_POST["value_id"];
				}else{
					$value = ($_POST["save"]=="true")? Value::create($_POST["label"], $f, md5(rand(0,1000))):Value::createTemp($_POST["label"], $f, "");
					$id = ($value->getId() != null)?$value->getId():"";
				}
				$_POST["log_in_rrd"] = (isset($_POST["log_in_rrd"]))?1:0; //Set the correct value for this checkbox 
				$value->setName($_POST["label"]);
				$value->setConfigData($_POST);
				echo json_encode(array(
					"error"=>false,
					"value"=>$value->getFormattedValue(),
					"id"=>$id
				));
			}else{
				echo json_encode($check);
			}
		}
		break;
	
	case "saveValueNameAssociation":
		if(isset($_POST["value_id"]) && isset($_POST["data"])){
			$value = Value::fromDatabase($_POST["value_id"]);
			$value->resetValueNames();
			foreach($_POST["data"] as $association){
				if($association["id"] != "")
					$value ->setValueName($association["id"],$association["name"]);
			}
			$value->storeToDatabase();
			echo json_encode(array(
				"error"=>false
			));
		}
		break;
	
	case "error":
	default:
		parameterError();
}

/**
 * Stores/updates an element in the database 
 * @param array $element Element Definition as passed by the save button of the layout interface
 */
function saveElement($element){
	global $_group_ID_matching, $_element_ID_matching, $db;
	if(!is_numeric($element["group"])){ //If the id is not yet numeric remove the "group_" prefix 
		$element["group"] = substr($element["group"],6);
	}
	if($element["group"]>=10000){
		$element["group"] = $_group_ID_matching[$element["group"]];
	}
	$additionalParameters = array();
	if(isset($element["target"])){
		$additionalParameters["target"]=$element["target"];
	}
	if(isset($element["rows"])){
		$additionalParameters["rows"]=$element["rows"];
	}
	if(!isset($element["name"])){
		$tmp = new Element($element["valueId"],$element["valueName"],$additionalParameters);
	}else{
		$tmp = new Element($element["valueId"],$element["name"],$additionalParameters);
	}
	$tmp->setStyle($element["style"]);
	$tmp->setClass($element["class"]);
	if(!is_numeric($element["id"])){ //If the id is not yet numeric remove the "group_" prefix 
		$element["id"] = substr($element["id"],8);
	}
	if($element["id"]<10000){
		$tmp->setElementId($element["id"]);
		//Id is set, update element
		$stmt = $db->prepare('UPDATE `elements` SET `group_id`=:group_id, `name`=:name, `display_object`=:display_object WHERE `id`=:id');
		$stmt->bindValue(':group_id', $element["group"], SQLITE3_INTEGER);
		if(!isset($element["name"])){
			$stmt->bindValue(':name', $element["valueName"], SQLITE3_TEXT);
		}else{
			$stmt->bindValue(':name', $element["name"], SQLITE3_TEXT);
		}
		$stmt->bindValue(':display_object', $tmp->enc(), SQLITE3_TEXT);
		$stmt->bindValue(':id', $element["id"], SQLITE3_INTEGER);
		$stmt->execute();
	}else{
		//Id is not set, create element and assign Id
		//Needs a new, unique id. Get all IDS and find a not used one for the new entry
		$stmt = $db->prepare('SELECT id FROM `elements`');
		$result = $stmt->execute();
		$existingIDs = array();
		while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
			$existingIDs[] = $row["id"];
		}
		$newUniqueId = $element["id"];
		for($i=1;$i<10000;$i++){
			if(!in_array($i,$existingIDs)){
				$newUniqueId = $i;
				$_element_ID_matching[$element["id"]] = $newUniqueId;
				break;
			}
		}
		$tmp->setElementId($newUniqueId);
		$stmt = $db->prepare('INSERT INTO `elements` (`id`,`group_id`,`name`,`value_id`,`display_object`) VALUES (:id,:group_id,:name,:value_id,:display_object)');
		$stmt->bindValue(':id', $newUniqueId, SQLITE3_INTEGER);
		$stmt->bindValue(':group_id', $element["group"], SQLITE3_INTEGER);
		if(!isset($element["name"])){
			$stmt->bindValue(':name', $element["valueName"], SQLITE3_TEXT);
		}else{
			$stmt->bindValue(':name', $element["name"], SQLITE3_TEXT);
		}
		$stmt->bindValue(':value_id', $element["valueId"], SQLITE3_INTEGER);
		$stmt->bindValue(':display_object', $tmp->enc(), SQLITE3_TEXT);
		$stmt->execute();
	}
//	debug($tmp);
}

/**
 * Stores/updates a Group in the database
 * @param array $group Group Definition as passed by the save button of the layout interface
 * @param int $layoutId The Id of the layout the group belongs to
 */

function saveGroup(&$group,$layoutId){
	global $_group_ID_matching, $db;
	if(!is_numeric($group["id"])){ //If the id is not yet numeric remove the "group_" prefix 
		$group["id"] = substr($group["id"],6);
	}
	if(!is_numeric($layoutId)){ //If the id is not yet numeric remove the "layout_" prefix 
		$layoutId = substr($layoutId,7);
	}
	
	$stmt = $db->prepare('SELECT id FROM `groups` WHERE id LIKE :id');
	$stmt->bindValue(':id', $group["id"], SQLITE3_INTEGER);
	$result = $stmt->execute();
	
	$return = array();
	while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
		$return[] = $row;
	}
	if(count($return)>0){
		//Found ID, updating
		$stmt = $db->prepare('UPDATE `groups` SET `layout_id`=:layout_id, `name`=:name, `position`=:position WHERE `id`=:id');
		$stmt->bindValue(':layout_id', $layoutId, SQLITE3_INTEGER);
		$stmt->bindValue(':name', $group["name"], SQLITE3_TEXT);
		$stmt->bindValue(':position', $group["style"], SQLITE3_TEXT);
		$stmt->bindValue(':id', $group["id"], SQLITE3_INTEGER);
		$stmt->execute();
		
	}else{
		//Needs to be inserted, therefore needs a new, unique id. Get all IDS and find a not used one for the new entry
		$stmt = $db->prepare('SELECT id FROM `groups`');
		$result = $stmt->execute();
		$existingIDs = array();
		while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
			$existingIDs[] = $row["id"];
		}
		$newUniqueId = $group["id"];
		for($i=1;$i<10000;$i++){
			if(!in_array($i,$existingIDs)){
				$newUniqueId = $i;
				break;
			}
		}
		$_group_ID_matching[$group["id"]] = $newUniqueId;
		
		
		$stmt = $db->prepare('INSERT INTO `groups` (`id`,`layout_id`,`name`,`position`) VALUES (:id,:layout_id,:name,:position)');
		$stmt->bindValue(':id', $newUniqueId, SQLITE3_INTEGER);
		$stmt->bindValue(':layout_id', $layoutId, SQLITE3_INTEGER);
		$stmt->bindValue(':name', $group["name"], SQLITE3_TEXT);
		$stmt->bindValue(':position', $group["style"], SQLITE3_TEXT);
		
		$stmt->execute();
		
	}
}

/**
 * @param int $groupId The id of the group to be removed
 * @return SQLite3Result
 */
function removeGroup($groupId){
	global $db;
	if(!is_numeric($groupId)){ //If the id is not yet numeric remove the "group_" prefix 
		$groupId = substr($groupId,6);
	}

	$stmt = $db->prepare('DELETE FROM `groups` WHERE id LIKE :id');
	$stmt->bindValue(':id', $groupId, SQLITE3_INTEGER);
	return $stmt->execute();
	
	
}

/**
 * Retrieves a value from an address depending on the connection type
 * @param string $connType Type of the connection 
 * @param string $address Address where the value shall be retrieved
 * @return string
 */
function getValueByAddress($connType,$address){
	switch($connType){
		case "OneWire":
			global $oneWire;
			$value = $oneWire->getValueFromAddress($address);
			break;
		case "KNX":
			global $knx;
			$value = $knx->getValueFromAddress($address);
			break;
		default:
			$value = null;
	}
	return json_encode(array("value"=>$value));
}

/**
 * Retrieves a Value by its id
 * @param int $id Id of the value 
 * @param bool $raw Return the raw value or also the formatted value
 * @return string Json Array
 */
function getValueById($id,$raw = false){
	$value = Value::fromDatabase($id);
	$colors = "";
	foreach($value->getColors() as $name=>$color){
		$colors .= $name.": ".$color.";";
	}

    $getValue = $value->getValue(); //only one request to the ownet at the same time

	if($raw){
		return json_encode(array("value"=>$getValue,"rawValue"=>$getValue,"valueName"=>$value->getNameForValue($getValue),"colors"=>$colors));
	}else{
		return json_encode(array("value"=>$value->getFormattedValue($getValue),"rawValue"=>$getValue,"valueName"=>$value->getNameForValue($getValue),"colors"=>$colors));
	}
}

/**
 * Echos a json array defining a parameter error
 */
function parameterError(){
	echo json_encode(array("error"=>true,"desc"=>"An error with a parameter occurred. "));
}

/**
 * Returns the formatted content for the overlay that was requested
 * @param string $request The requested page
 * @return string Formatted string
 */
function getContentForOverlay($request){
	global $db;
	switch($request){
		case "getAllDoubleValues":
			$stmt = $db->prepare('SELECT id,name,object FROM `values` ORDER BY `name` DESC');
			$result = $stmt->execute();
			$return = array();
			while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
				$tmp = Value::dec($row["object"]);
				if($tmp->getValueType() == "double" || $tmp->getValueType() == "integer" ){
					$return[$row["id"]] = $row["name"];
				}
			}
			return ulWithOnclickAction($return,"setValue");
			break;
		
		case "getAllIntegerValues":
			$stmt = $db->prepare('SELECT id,name,object FROM `values` ORDER BY `name` DESC');
			$result = $stmt->execute();
			$return = array();
			while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
				$tmp = Value::dec($row["object"]);
				if($tmp->getValueType() == "integer"){
					$return[$row["id"]] = $row["name"];
				}
			}
			return ulWithOnclickAction($return,"setValue");
			break;
		
		case "getAllBoolValues":
			$stmt = $db->prepare('SELECT id,name,object FROM `values` ORDER BY `name` DESC');
			$result = $stmt->execute();
			$return = array();
			while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
				$tmp = Value::dec($row["object"]);
				if($tmp->getValueType() == "boolean"){
					$return[$row["id"]] = $row["name"];
				}
			}
			return ulWithOnclickAction($return,"setValue");
			break;
		
		case "getAllWriteableBoolValues":
			$stmt = $db->prepare('SELECT id,name,object FROM `values` ORDER BY `name` DESC');
			$result = $stmt->execute();
			$return = array();
			while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
				$tmp = Value::dec($row["object"]);
				if($tmp->getValueType() == "boolean" && $tmp->isWriteable()){
					$return[$row["id"]] = $row["name"];
				}
			}
			return ulWithOnclickAction($return,"setValue");
			break;
		
		case "showAssignedNamesForValue":  //Shows a selector that displays the currently assigned values for an integer or boolean 
			if(isset($_POST["value_id"])){
				$val = Value::fromDatabase($_POST["value_id"]);
				$output = "<table>";
				$output .= "<tr><th>Number</th><th>Definition</th></tr>";
				foreach($val->getAllValueNames() as $id=>$name){
					$output .= "<tr><td><input type='number' value='".$id."' name='id' /></td><td><input type='text' value='".$name."' name='name'  /></td></tr>";
				}
				$output .= "</table>";
				$output .= "<input type='button' value='Add' onclick='addValueNameAssociation(this)'/>";
				$output .= "<input type='hidden' name='value_id' value='".$_POST["value_id"]."' />";
				$output .= "<input type='button' value='Save' onclick='saveValueNameAssociation(this)'/>";
				echo $output;
			}else{
				echo "No value id passed, aborting";
			}
			break;
		case "showBoolAssociation":
			$possibleColors = array("grey"=>"Grey","green"=>"Green","red"=>"Red","orange"=>"Orange","blue"=>"Blue");
			if(isset($_POST["value_id"])) {
				$val = Value::fromDatabase($_POST["value_id"]);
				$association = $val->getAllValueNames();
				if(count($association) != 2){
					//Not exactly two associations (boolean is always two) reset to a default state
					$association = array(0=>"grey",1=>"green");
				}
				$output = "Select which color the notification icon should display,<br /> depending on the value of the variable. <br />";
				foreach($association as $id=>$color){
					if($id == 0){
						$output .= "False: ";
					}else{
						$output .= "True: ";
					}
					$output .= generateSelect("color_".$id,$possibleColors,$color)." <br />";
				}
				$output .= "<input type='hidden' name='value_id' value='".$_POST["value_id"]."' />";
				$output .= "<input type='button' value='Save' onclick='saveBoolAssociation(this)'/>";
				echo $output;
			}else{
				echo "No value id passed, aborting";
			}
			break;
		case "showLimitsForValue":
			if(isset($_POST["value_id"])) {
				$val = Value::fromDatabase($_POST["value_id"]);
				$output = "Define limits for <b>'".$val->getName()."'</b><br />";
				$output .= "<input type='button' value='Add Limit' onClick='addLimit(".$_POST['value_id'].")' /><div id='limits'>";
				$limits = Limit::getLimitsForValueId($_POST["value_id"]);
				foreach($limits as $id=>$limit){
					$output .= $limit->getEdit();
				}
				$output .= "</div>";
				$output .= "<input type='button' value='Save' onClick='saveLimits(".$_POST['value_id'].")'>";
				echo $output;
				
			}else{
				echo "No value id passed, aborting";
			}
			break;
		case "addLimitToValue":
			if(isset($_POST["value_id"])) {
				if(isset($_POST["limit_id"])) {
					//Overwrite limit
					echo "overwrite limit";
					$limit = Limit::getLimitById($_POST["limit_id"],$_POST["value_id"]);
					if(isset($_POST['lower'])){
						$lower = floatval($_POST['lower']);
						$upper = floatval($_POST['upper']);
					}else{
						$lower = null;
						$upper = ($_POST["limit"]=="true")?true:false;
					}
					$limit->setLimits($lower,$upper);
					
				}elseif(isset($_POST['lower']) || isset($_POST['limit'])){
					//Save as new limit
					echo "new limit";
					$limit = new Limit($_POST["value_id"]);
					if(isset($_POST['lower'])){
						$lower = floatval($_POST['lower']);
						$upper = floatval($_POST['upper']);
					}else{
						$lower = null;
						$upper = ($_POST["limit"]=="true")?true:false;
					}
					$limit->setLimits($lower,$upper);
				}else{
					//Nothing requested, just return empty limit definer
					echo Limit::addLimitToValue($_POST["value_id"]);
				}
			}else{
				echo "No value id passed, aborting";
			}
			break;
		
		case "showActionsForLimit":
			if(isset($_POST["limit_id"])) {
				foreach(Limit::possibleActions as $name=>$action){
					echo "<input type='button' value='".$action["name"]."' onClick='addAction(this,".$_POST["limit_id"].",\"".$name."\")' />";
				}
				$limit = Limit::getLimitById($_POST["limit_id"],0);
				echo "<br /><div class='actions'>";
				foreach($limit->getActions() as $id=>$action){
					echo "<div>".$action->getConfiguration()."</div>";
				}
				echo "</div>";
				echo "<input type='button' value='Save' onClick='saveActions(this,".$_POST["limit_id"].")' />";
			}else{
				echo "No limit id passed, aborting";
			}
			break;
		
		case "addActionToLimit":
			if(isset($_POST["limit_id"])) {
				if(isset($_POST["action"])){
					$actionString = Limit::possibleActions[$_POST["action"]]["className"];
					$action = new $actionString($_POST["limit_id"]);
					echo "<div>".$action->getConfiguration()."</div>";
				}else{
					echo "no action given";
				}
			}else{
				echo "No limit id passed, aborting";
			}
			break;
		
		case "saveActionToLimit":
			if(isset($_POST["limit_id"])) {
				$limit = Limit::getLimitById($_POST["limit_id"],0);
				$limit->removeActions();
				if(isset($_POST["actions"])){
					foreach($_POST["actions"] as $id=>$actionArray){
						$actionString = Limit::possibleActions[$actionArray["action"]]["className"];
						$action = new $actionString($_POST["limit_id"]);
						$action->saveSettings($actionArray);
						$limit->addAction($action);
					}
				}
				$_POST["value_id"] = Limit::getLimitById($_POST["limit_id"],0)->getValueId();
				echo "Actions saved";
			}else{
				echo "No limit id passed, aborting";
			}
			break;
		case "getAllGraphs":
			$stmt = $db->prepare('SELECT * FROM `graphs`');
			$result = $stmt->execute();
			$graphs = array();
			while($row = $result->fetchArray(SQLITE3_ASSOC)){
				$graphs[$row["id"]] = $row;
			}
			$availableGraphs = array();
			foreach($GLOBALS["graphsTimeframes"] as $nameTime=>$startTime){
				foreach($graphs as $graph){
					$availableGraphs[$graph["id"]]["name"] = $graph["name"];
					$availableGraphs[$graph["id"]]["timeFrames"][$nameTime] = "graph_".$graph["id"]."_".$nameTime.".png";
				}
			}
			foreach($availableGraphs as $graphId=>$graph){
				echo $graph["name"]."<br />";
				$segments = array();
				foreach($graph["timeFrames"] as $frameName=>$filePath){
					$segments[] = "<span type='button' style='cursor: pointer;' onclick='setGraph(\"".$graphId."\",\"".$frameName."\",\"".$filePath."\")'>".$frameName."</span>";
				}
				echo implode(" | ",$segments)."<br />";
			}
			break;
		case "getButtonActions":
			//List all possible layouts
			echo "Set the action this button shall perform<br />";
			//Clicking on one of the layouts automatically prefills the custom textfield
			echo "Choose one of the layouts as target: <br />";
			$stmt = $db->prepare('SELECT id,name FROM `layouts`');
			$result = $stmt->execute();
			$layouts = array();
			while($row = $result->fetchArray(SQLITE3_ASSOC)){
				$layouts[$row["id"]] = $row["name"];
			}
			foreach($layouts as $id=>$name){
				echo "<input type='button' value='".$name."' onClick=\"selectLayout(".$id.",'".$name."')\"/>";
			}
			echo "<br />";
			//Textfield for Label
			echo "Label:&nbsp;&nbsp;<input type='text'  name='label' placeholder='Button' /><br />";
			//Textfield for custom url
			echo "Target:&nbsp;<input type='text' name='target' value='' placeholder='http://wherever.you/go' /><br />";
			echo "<input type='button' value='Save' onClick='saveButtonSettings(this)' />";
			//Maybe allow selection of element for overlay (e.g. graph). 
			
			break;
		case "getLogActions":
			global $defaultLogger; 
			//List all possible layouts
			echo "Set the Log Level for this Log Window<br />";
			$logLevels = $defaultLogger->getLogLevels();
			//$logLevels[0] = "ALL";  //Insert additional level that displays all levels
			echo "Max number of rows to display (type -1 for all): <input type='text' value='30' name='rows' placeholder='Rows' /><br />";

            foreach($logLevels as $level=>$name){
                echo "<input type='button' value='".$name."' onClick=\"this.style.backgroundColor='dodgerblue'; setLogData(0, '$level');\">";

            }

            echo "<input type='button' value=' Save ' style=\"background-color:limegreen; margin-left:15px;\" onClick=\"setLogData(1, 0); selectLogLevel(this, setLogLevel, setLogName,$(this).parent().find('input[name=rows]').val())\"/>";

            //Maybe allow selection of element for overlay (e.g. graph).
			
			break;
	}
}

/**
 * @param array $data ID->Value pair array for clickable interactions
 * @param string $onclick The name of the Javascript function to be called with the parameters
 * @return string Unordered List object with all array elements as list objects
 */
function ulWithOnclickAction($data,$onclick=""){
	$return = "<ul>";
	foreach($data as $id=>$name){
		$return .= "<li type='button' onclick='".$onclick."(\"".$id."\",\"".$name."\")'>".$name."</li>";
	}
	return $return."</ul>";
}
