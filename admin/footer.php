<?php
/**
 * Main Footer that displays the memory usage and rendering time
 * 
 */
echo "<div style='height:30px; float: left; width: 100%;'>&nbsp;</div>"; //Empty space so the fixed thing doesn't overlap the content without it being scrollable
echo "<div style='position: fixed; left:0; bottom: 0; padding:5px; padding-left: 15px; width: 100%; background-color: #002a80; color: #ffffff; z-index: 1000'>";
echo "Memory usage: ".formatBytes(memory_get_usage(true))." ";
echo "Rendering took ".sprintf("%1.2f seconds",microtime(true)-$globalStartTime)."<br />";
echo "</div>";
$db->close();
unset($db);
