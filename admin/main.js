function displaySelectorOverlay(data,target){
    var left = target.offset().left+target.outerWidth();
    var top = target.offset().top+target.outerHeight();
    valueTarget = target; //Set the global variable so the setValue function knows what to update
    var content = "<div id='overlay' style='position: absolute; display: block; left:"+left+"px; top:"+top+"px;'>"+data+"</div>";
    var closeButton = "<div id='closeButton' style='position: absolute; display: block; left:"+(left-10)+"px; top:"+(top-10)+"px; background-color: white; z-index: 101; border-radius: 15px; width: 17px; text-align: center; cursor: pointer' onclick='closeSelectorOverlay()'>x</div>";
    $("#overlay").remove();
    $("#closeButton").remove();
    $("body").append(content);
    $("body").append(closeButton);
    // $("#overlay").draggable();  //Just an Idea to make the window moveable, would require a different way of presenting the close Button since it is also fixed right now. 
}


function closeSelectorOverlay(){
    $("#overlay").fadeOut(200,function(){$(this).remove();});
    $("#closeButton").fadeOut(200,function(){$(this).remove();});
}


