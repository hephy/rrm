<?php
/**
 * Created by PhpStorm.
 * User: Benedikt Würkner
 * Date: 21/03/16
 * Time: 16:00
 */

/**
 * Main Index file of the Admin interface
 */
require "header.php";
require "Element.php";
require "Group.php";
require "Limit.php";
require "../lib/DataSources/LocalValue.php";
require "../lib/DataSources/Formula.php";


echo "<div style='position: fixed; left:0; top: 0; padding:5px; padding-left: 15px; width: 100%; background-color: #002a80; color: #ffffff; z-index: 1000;'>";
echo "<a href='?page=parameters' style='color: #fff;'>Parameters</a> | <a href='?page=values' style='color: #fff;'>Values</a> | <a href='?page=graphs' style='color: #fff;'>Graphs</a> | <a href='?page=layout' style='color: #fff;'>Layout</a>";
echo "</div>";
echo "<div style='height:30px;'>&nbsp;</div>"; //Empty space so the fixed thing doesn't overlap the content without it being scrollable


 
$page = (isset($_GET["page"]))? $_GET["page"]:"layout";

switch($page){
	case "parameters":
		require "views/config_params.html";
		renderParameters();
		break;

	case "values":
		require "views/config_values.html";
#		echo "<br /><br />List of all Values. Allows to assign a unit, add formulas, set limits for alarms and alarm messages, tag values that should be stored in the rrd, set the amount of displayed decimal places. ";
		$stmt = $db->prepare('SELECT id,objectType FROM `values`');
		$result = $stmt->execute();
		$values = array();
		$valueTypes = array();
		$formulas = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$values[$row["id"]] = Value::fromDatabase($row["id"]);
			if($row["objectType"] == "Formula"){
				$formulas[$row["id"]] = $values[$row["id"]];
			}else{
				$valueTypes[$values[$row["id"]]->getValueType()][] = $row["id"];
			}
		}
		echo "<div id='tabs' style='display: inline-block; float:left;'>";
		echo "<ul>";
		$count = 0;
		foreach($valueTypes as $type=>$ids){
			echo "<li><a href='#l".$count."'>".$type."</a></li>";
			$count++;
		}
		echo "<li><a href='#l".$count."'>Formulas</a></li>";
		echo "</ul>";
		$count = 0;
		foreach($valueTypes as $type=>$ids){
			echo "<div id='l".$count."'><input type='button' value='Add Value' onClick='addValue(this,\"".$type."\")'><table>
				<tr>
					<th>ID</th>
					<th>Raw Value</th>
					<th>Preview Value</th>
					<th>Decimals</th>
					<th>Unit</th>
					<th>Internal Label</th>
					<th>Log in RRD</th>
					<th>&nbsp;</th>
				</tr>";
			foreach($ids as $id){
				$value = $values[$id];
				echo "<tr>";
					echo "<td>".$id."<input type='hidden' value='".$id."' name='value_id' /></td>";
#					echo "<td style='text-align: right;'>".$value->getValueType()."</td>";
					echo "<td class='value'></td>"; //".$value->getValue()." </td>"; //TODO: Find a way to make the loading of the values only happen after the page has loaded so the creation of the page doesn't take 2.8+ seconds
					echo "<td class='previewValue'></td>"; //".$value->getFormattedValue()."</td>";  //TODO: See row above
					echo "<td><input type='number' placeholder='Decimals' value='".$value->getConfigData()["number_of_decimals"]."' name='number_of_decimals' size='1'/></td>";
					echo "<td><input type='text' value='".$value->getConfigData()["unit"]."' name='unit' size='5' placeholder='Unit' /></td>";
					echo "<td><input type='text' value='".$value->getName()."' name='label' size='20' /></td>";
					$checked = ($value->getConfigData()["log_in_rrd"]==1)?"checked='checked'":"";
					echo "<td><input type='checkbox' name='log_in_rrd' ".$checked." /></td>";
					echo "<td><input type='button' value='Save' onclick='saveValues(this)'/>";
					echo "<input type='button' value='Delete' onclick='removeValue(this)'/>";
					if($type == "integer")
						echo "<input type='button' value='Value Association' onclick='valueNameAssociation(this)'/>";
					elseif($type == "boolean")
						echo "<input type='button' value='Boolean Meaning' onclick='booleanMeaningAssociation(this)'/>";

					echo "<input type='button' name='limits_".$id."' value='Limits/Actions' onclick='limitAssignment(this)'/>";
					echo "(".+count(Limit::getLimitsForValueId($id))." set)";
					echo "</td>";
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>";
			$count++;
		}
		echo "<div id='l".$count."'>Formulas <input type='button' value='Add' onClick='addFormula()' /><br />To use a value in a formula put it's ID in curly braces e.g. Value1 = '{1}', Example of a formula: ({1}+{2})/2";
		echo "<table id='formulas'>";
			echo "<tr><th>ID</th><th>Formula</th><th>Decimals</th><th>Unit</th><th>Value Preview</th><th>Label</th><th>Log in RRD</th><th>&nbsp;</th></tr>";
			foreach($formulas as $id=>$formula){
				echo "<tr>";
					echo "<td>".$id."<input type='hidden' value='".$id."' name='value_id' /></td>";
					echo "<td><input type='text' placeholder='Formula' name='formula' value='".$formula->getDatasource()->getRawFormula()."' /></td>";
					echo "<td><input type='number' placeholder='Decimals' value='".$formula->getConfigData()["number_of_decimals"]."' name='number_of_decimals' size='1'/></td>";
					echo "<td><input type='text' value='".$formula->getConfigData()["unit"]."' name='unit' size='5' /></td>";
					echo "<td><input type='text' name='preview' value='".$formula->getFormattedValue()."' disabled='disabled'/></td>";
					echo "<td><input type='text' name='label' value='".$formula->getName()."' placeholder='Label' /></td>";
					$checked = ($formula->getConfigData()["log_in_rrd"]==1)?"checked='checked'":"";
					echo "<td><input type='checkbox' name='log_in_rrd' ".$checked." /></td>";
					echo "<td><input type='button' value='Preview' onclick='FormulaValue(this)' />";
					echo "<input type='button' value='Save' onclick='FormulaValue(this,true)' />";
					echo "<input type='button' value='Delete' onclick='removeValue(this)'/>";
                    if($formula->getValueType() == "boolean")
                    {
                        echo "<input type='button' value='Boolean Meaning' onclick='booleanMeaningAssociation(this)'/>";
                    }else
                    {
                        echo "<input style=\"text-decoration:line-through;\" type='button' value='Boolean Meaning''/>";
                    }
					echo "<input type='button' name='limits_".$id."' value='Limits/Actions' onclick='limitAssignment(this)'/>";
					echo "(".+count(Limit::getLimitsForValueId($id))." set)";
					echo "</td>";
				echo "</tr>";
				
			}
		echo "</table></div>";
		echo "</div>";
		
		break;

	case "layout":
		require "views/layout_modifier.html";
		echo "<script type='text/javascript'>var graphPath = \"../output/rrd_scripts/\";</script>";
		
		$stmt = $db->prepare('SELECT id,name FROM `layouts`');
		$result = $stmt->execute();
		$layouts = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$layouts[$row["id"]] = $row["name"];
		}
		echo "<script type='text/javascript'>var layouts = $.parseJSON('".json_encode($layouts)."');</script>";
		
		echo "<div id='tabs' style='display: inline-block; float:left;'>";
		echo "<ul>";
		foreach($layouts as $layoutId=>$layoutName){
			echo "<li><a href='requestHandler.php?r=renderLayout&layout_id=".$layoutId."'>".$layoutName."</a></li>";
		}
		echo "<li><a href='#l".($layoutId+1)."'>New</a></li>";
		echo "</ul>";
//		foreach($layouts as $layoutId=>$layoutName){
//			renderLayout($layoutId);
//		}
		renderLayout($isNew = True, $layoutId+1);
		echo "</div>";
		echo '<div id="droppables">
				<div id="positionDisplay"></div>
				<div id="trashGroups">
				</div>
				<div id="trashElements">
				</div>
				<div class="group new-group" style="width: 100px; height: 50px; position: relative;">
					<div class="name">Name</div>
				</div>
				<div class="element new-element el-value el-obstacle el-value-left">
					<span class="valueName">Label</span><span class="valueValue">value</span>
				</div>
				<div class="element new-element el-value el-obstacle el-value-left el-small">
					<span class="valueName">Label</span><span class="valueValue">value</span>
				</div>
				<div class="element new-element el-value el-obstacle el-value-top">
					<span class="valueName">Label</span><span class="valueValue">value</span>
				</div>
				<div class="element new-element el-value el-obstacle el-value-top el-small">
					<span class="valueName">Label</span><span class="valueValue">value</span>
				</div>
				<div class="element new-element el-value el-obstacle el-value-top el-input">
					<span class="valueName">Label</span><span class="valueValue">value</span>
				</div>
				<div class="element new-element el-selection el-obstacle">
					<span class="valueName">Label</span><span class="valueValue"><select><option>option</option></select></span>
				</div>
				<div class="element new-element el-graph el-obstacle">
					Graph
				</div>
				<div class="element new-element el-logwindow el-obstacle">
					<span class="valueName">Log <br />Window</span>
				</div>
				<div class="element new-element el-button el-obstacle">
					<span class="valueName">Button</span>
				</div>
				<div class="element new-element el-notification el-obstacle">
					<span class="valueValue">&nbsp;</span><span class="valueName">Label - Readonly</span>
				</div>
				<div class="element new-element el-notification el-obstacle el-input">
					<span class="valueValue">&nbsp;</span><span class="valueName">Label - Read/Write</span>
				</div>
			</div>';
		echo "<div id='preview'></div>";
		break;

	case "graphs":
		require "GraphConfiguration.php";
		require "views/config_graphs.html";
		
		//Display all possible values (double and formulas) in the top right corner to make it easier for the user to get the correct corresponding values. 
		$stmt = $db->prepare('SELECT id,objectType FROM `values` WHERE store_in_rrd = 1 ORDER BY name');
		$result = $stmt->execute();
		$values = array();
		$doubles = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$values[$row["id"]] = Value::fromDatabase($row["id"]);
			if($row["objectType"] == "Formula") {
				$doubles[$row["id"]] = $values[$row["id"]];
			}elseif($values[$row["id"]]->getValueType() == "double"){
				$doubles[$row["id"]] = $values[$row["id"]];
			}
		}
		echo "<div style='position: fixed; right: 0px; top:30px; background-color: white; border: solid 1px grey; border-radius: 5px;' id='valueInfo'>";
		echo "<h3>Value IDs and paths</h3>";
		echo "<div>";
		echo "<a href='http://oss.oetiker.ch/rrdtool/doc/rrdgraph_graph.en.html' target='_blank'>Graph configuration definition</a>";
		echo "<table>";
		echo "<tr><th>ID</th><th>Name</th><th>variablePath</th></tr>";
		foreach($doubles as $id=>$value){
			echo "<tr><td>".$id."</td><td>".$value->getName()."</td><td><input type='text' value='{rrdDatabases}value_".$id.".rrd:value".$id.":AVERAGE' style='border: solid 0px black;' onClick='$(this).select()'/> </td></tr>";
		}
		echo "</table></div>";
		echo "</div>";
		
		$stmt = $db->prepare('SELECT * FROM `graphs`');
		$result = $stmt->execute();
		$graphs = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$graphs[$row["id"]] = $row;
		}
		$G = new GraphConfiguration();
		foreach($graphs as $graph){
			echo $G->getEditView($isNew = false, $graph);
		}
#		echo $G->createConfiguration(1,-3000,"Test2","bla",800,200,"DEF:t02=/var/www/html/output/rrd_databases/value_2.rrd:value2:AVERAGE LINE1:t02#ff0000:\" Value 02\"");
		echo "<h3>New</h3>";
		echo "Press Save to store, this will reload the page as well and save the new configuration<br />";
		echo $G->getEditView($isNew = true); //New Graph at the End
//		$images = array("diffpressure_6h.png","smcpressure_6h.png","smcvak_6h.png","tempout_6h.png","temp_6h.png");
//		foreach($images as $graph){
//			echo "<img src='http://192.168.0.165/output/rrd_scripts/".$graph."'>";
//			echo "<img src='http://reinraummon.local.hephy.at/graph/".$graph."'><br />";
//		}
		break;
	default:
		break;
}


require "footer.php";


exit();


/**
 * Function that renders all parameters
 */
function renderParameters(){
	global $db,$oneWire,$gpio;
	//Get all Parameters stored in the Database
	$stmt = $db->prepare('SELECT id,`name`,address,objectType FROM `values` ORDER BY `name`');
	$result = $stmt->execute();
	$tmp = array();
	while($row = $result->fetchArray(SQLITE3_ASSOC)){
		$tmp[] = $row;
	}
	$valuesByType = array("OneWire"=>array());	
	foreach($tmp as $value){
		$valuesByType[$value["objectType"]][$value["address"]] = $value;
	}
	$setOneWireDevices = array();  //Array with all the One Wire Devices that are set in the same format as the array with all in general
	foreach($valuesByType["OneWire"] as $i=>$value){
		$tmp = splitOneWireAddressIntoArray($value);
		$deviceCode = array_keys($tmp)[0];
		if(isset($setOneWireDevices[$deviceCode])){
			$setOneWireDevices[$deviceCode] = array_merge($tmp[$deviceCode],$setOneWireDevices[$deviceCode]);
		}else{
			$setOneWireDevices[$deviceCode] = $tmp[$deviceCode];
		}
	}
#	debug($setOneWireDevices);
	//Get all Devices Attached to 1Wire
	$allOneWireDevices = array();
	foreach($oneWire->getAvailableDevices() as $device){
		$thisDevice = $oneWire->getAllParametersOfDevice($device,$valuesByType["OneWire"]);
		if(is_array($thisDevice)){
			$allOneWireDevices = array_merge($allOneWireDevices,$thisDevice);
		}
	}
#	file_put_contents("tmp.txt",json_encode($allOneWireDevices));
//	debug($allOneWireDevices);
//	runtime("generate array with devices");
	
	//Get all possible GPIO Pins
	
	//Get all configured KNX Devices
	$stmt = $db->prepare("SELECT id,objectType FROM `values` WHERE `objectType` = 'KNX'");
	$result = $stmt->execute();
	$knxValues = array();
	while($row = $result->fetchArray(SQLITE3_ASSOC)){
		$knxValues[$row["id"]] = Value::fromDatabase($row["id"]);
	}
	
	$valueTypes = array(
		"double"=>"Float",
		"integer"=>"Integer",
		"boolean"=>"True/False",
		"string"=>"String",
		"NULL"=>"NULL",
	);

	//Generate output
	echo "<div id='tabs' style='display: inline-block; float:left;'>";
	echo "<ul>
			<li><a href=\"#OneWire\">One Wire</a></li>
			<li><a href=\"#GPIO\">GPIO</a></li>
			<li><a href=\"#KNX\">KNX</a></li>
			<li><a href=\"#set_values\">Assigned Values</a></li>
		  </ul>";
	echo "<div id='OneWire' class='datasource'>";
//	echo "<h3>One Wire</h3>";
	echo recursiveSubelementGenerator($allOneWireDevices);
	echo runtime("OneWire");
	echo "</div>";
	$stmt = $db->prepare("SELECT id,objectType FROM `values` WHERE `objectType` = 'GpioPHP'");
	$result = $stmt->execute();
	$gpioValues = array();
	while($row = $result->fetchArray(SQLITE3_ASSOC)){
		$val = Value::fromDatabase($row["id"]);
		$gpioValues[$val->getAddress()] = $val;
	}

	echo "<div id='GPIO' class='datasource'>";
	echo "<table id='gpio'>";
	echo "<tr><th>ID</th><th>Pin Number</th><th>Label</th><th>Direction</th><th>&nbsp;</th></tr>";
	foreach($gpio->getAllPins() as $id=>$pinData){
		$name = (isset($gpioValues[$id]))?$gpioValues[$id]->getName():"";
		$parameter_id = (isset($gpioValues[$id]))?$gpioValues[$id]->getId():"";
		echo "<tr>";
		if($parameter_id != ""){
			echo "<td>".$parameter_id."<input type='hidden' value='".$parameter_id."' name='parameter_id' /></td>";
		}else{
			echo "<td>&nbsp;</td>";
		}
		echo "<td>".$id."<input type='hidden' value='".$id."' name='gpio_id' /></td>";
		echo "<td><input type='text' name='label' value='".$name."' placeholder='Label' /></td>";
		echo "<td>";
		echo generateSelect("direction",array("in"=>"in","out"=>"out"),$pinData["direction"]);
		echo "</td>";
		echo "<td>";
		echo "<input type='button' value='Save' onclick='GPIOValue(this)' /> <span class='error'></span></td>";
		echo "</td>";
		echo "</tr>";
	}
	echo "</table>";
	echo runtime("GPIO");
	echo "</div>";
	
	//Generate the select for javascript usage
	echo "<script type='text/javascript'>var valueTypes = \"".generateSelect("valueType",$valueTypes)."\"</script>";
	
	echo "<div id='KNX' class='datasource'>";
//	echo "<h3>KNX</h3>";
	echo "<input type='button' value='Add' onClick='addKNX()' />";
	echo "<table id='knx'>";
	echo "<tr><th>ID</th><th>Address</th><th>Value Preview</th><th>Label</th><th>Value Type</th><th>Writeable</th><th>&nbsp;</th></tr>";
	foreach($knxValues as $id=>$knxValue){
		echo "<tr>";
		echo "<td>".$id."<input type='hidden' value='".$id."' name='parameter_id' /></td>";
		echo "<td><input type='text' placeholder='Address' name='address' value='".$knxValue->getAddress()."' /></td>";
		echo "<td><input type='text' name='preview' value='' disabled='disabled'/></td>";
		echo "<td><input type='text' name='label' value='".$knxValue->getName()."' placeholder='Label' /></td>";
		echo "<td>";
		echo generateSelect("valueType",$valueTypes,$knxValue->getValueType());
		echo "</td>";
		$checked = ($knxValue->isWriteable())?"checked='checked'":"";
		echo "<td style='text-align: center;'><input type='checkbox' name='writeable' ".$checked."/>";
		echo "<td><input type='button' value='Preview' onclick='KNXValue(this)' />";
		echo "<input type='button' value='Save' onclick='KNXValue(this,true)' /> </td>";
		echo "</tr>";
		
	}
	
	echo "</table>";
	echo "</div>";
	echo "<div id='set_values' style='display: inline-block; float:right;'>";
		foreach($valuesByType as $dataSource=>$data){
			if($dataSource=="Formula") continue; //Skip formulas
			echo "<table width='100%' id='".$dataSource."_values'>";
			echo "<tr><th>&nbsp;&nbsp;&nbsp;</th><th>&nbsp;</th><th class='datasource'>".$dataSource."</th><th width='135'>&nbsp;</th></tr>";
			foreach($data as $address=>$value){
				echo "<tr><td>".$value["id"]."</td><td value_id='".$value["id"]."' class='value_name'>".$value["name"]."</td><td>".$address."</td><td>";
				if($dataSource=="OneWire"){
					echo "<input type='button' value='rename' onClick='RenameValue(".$value["id"].")'/>";
				}
				echo "<input type='button' value='Remove' onClick='RemoveValue(".$value["id"].",this)'/>";
				echo "</td></tr>";
			}
			echo "</table>";
		}
	echo "</div>";
	echo "</div>";  //Closes Tabs
	
#	runtime("generate list");
}

/**
 * Recursive function that generates ul and li elements of the hierarchy array of the elements. 
 * If a name of an element is set the element is recognized as already stored as a value and is treated as such
 * @param array $array array that is to be used as the basis of the recursion. If the "address" element is set it is treated as a parameter/value otherwise as an array that is to be recursed again
 * @param string $name The name to be used for this iteration (and displayed as such)
 * @param int $level The level of the iteration, defaults to 0 and only defines the starting point. Is used for a class level_$level to enable hierarchy styling. 
 * @return string The hierarchy. 
 */
function recursiveSubelementGenerator($array, $name = "", $level=0){
	global $setOneWireDevices;
	$return = "";
	if(isset($array["address"]) && !is_array($array["address"])){
		//Is a final element,
		//check if a name is set and if yes name the element accordingly
		$return .= "<li class='level_".$level."'>";
		$return .= "<span class='li_box_1'>".$array["parameter"]." <span class='errorBox'></span></span>";
		if(isset($array["name"])) {
			//Parameter is already assigned to a Value
			$return .= "<span class='li_box_2'><input type='text' value='".$array["name"]."' class='value_name' /> <input type='button' value='Update' class='assign_value' address='".$array["address"]."' parameter_id='".$array["id"]."'/></span>";
		}else{
			//Parameter is NOT assigned to a Value
			$return .= "<span class='li_box_2'><input type='text' value='' class='value_name' /> <input type='button' value='Assign' class='assign_value' address='".$array["address"]."'/></span>";
		}
		$return .= "<span class='li_box_3'><input type='button' value='Display value' class='display_value' address='".$array["address"]."'/><input type='text' disabled='disabled' class='output_value' /></span>";
		$return .= "</li>";
		//
	}else{
		//Is not yet a final element, create hierarchy
		if($name != ""){
			$return .= "<li class='level_".$level."'><span class='collapsible level_".$level."'>".$name;
#			debug($setOneWireDevices);
			if(isset($setOneWireDevices[trim($name)])){
				$return .= " - Set Values: ".implode(", ",array_keys($setOneWireDevices[$name]));
			}
			$return .= "</span>";	
		} 
		$return .= ($level>0)?"<ul style='display: none;'>":"<ul>"; //Hide subelements by default
		$level += 1;
		foreach($array as $name=>$item){
			$return .= recursiveSubelementGenerator($item,$name,$level);
		}
		$return .= "</ul></li>"; 
	}
	return $return;
}

/**
 * Splits the one wire addresses according to their separator in an array
 * @param array $array Array of one wire addresses
 * @return array Array of split up one wire addresses
 */
function splitOneWireAddressIntoArray($array){
	$address = explode("/",$array["address"]);
	$address = array_reverse($address);
	$return = recursiveAddLevelToArray($address,$array);
	return $return;
}

/**
 * Recursive function that adds one level to an array
 * @param array $address Array with all addresses at this level
 * @param array $data Array with all the data that can be used
 * @return array
 */
function recursiveAddLevelToArray($address,$data){
	$return = array();
	if(count($address)>0){
		$tmp = array_pop($address);
		if($tmp == ""){
			$tmp = array_pop($address);
		} 
		$return[$tmp] = recursiveAddLevelToArray($address,$data);
	}else{
		$return = $data;
	}
	return $return;
}




