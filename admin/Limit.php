<?php

/**
 * Defines a Limit for a Value and allows actions to be attached.
 * 
 * User: Benedikt Würkner
 * 
 * 
 */

require_once "../lib/Actions/EmailAction.php";
require_once "../lib/Actions/ColorAction.php";
require_once "../lib/Actions/ValueAction.php";
require_once "../lib/Actions/LogAction.php";

//Render Interface for a given ID

//Set Limit (can be upper or lower or both)

//Add Actions to a limit (can be one or multiple of the possible actions, some actions can even be used multiple times)
/**
 * Class Limit
 */
class Limit{
	
	private $lowerLimit = null;
	private $upperLimit = null;
	private $id;
	private $value_id;
	private $actions = array();
	/**
	 * @var array $possibleActions Definition of the classnames and display names of all possible actions 
	 */
	const possibleActions = array(
		"email"=>array("className"=>"EmailAction","name"=>"Email"),
		"color"=>array("className"=>"ColorAction","name"=>"Color"),
		"setValue"=>array("className"=>"ValueAction","name"=>"Set Value"),
		"log"=>array("className"=>"LogAction","name"=>"Log"),
	);
	private $checkCounter = 0;
	private $resetCounter = 0;
	
	function __construct($valueId)
	{
		$this->value_id = $valueId;
		
	}
	
	public function delete(){
		global $db;
		$stmt = $db->prepare('DELETE FROM `limits` WHERE id LIKE :id');
		$stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
		$stmt->execute();
	}
	
	public static function addLimitToValue($valueId){
		$val = Value::fromDatabase($valueId);
		$return = "";
		if($val->getValueType() == "boolean"){
			//Value is a boolean, offer actions for the cases of true and false
			$return .= "<div><select name='limit'><option value='true'>true</option><option value='false'>false</option></select> You have to save before you can add actions</div>";
		}elseif($val->getValueType() == "double" || $val->getValueType() == "integer"){
			//Value is a double or integer, offer actions when the value raises above or sinks below a defined value
			$return .= "<div><input type='number' name='lower' /> < <input type='number' name='upper' /> You have to save before you can add actions</div>";
		}
		return $return;
	}
	
	public static function getLimitById($limitId,$valueId){
		global $db;
		$stmt = $db->prepare('SELECT id, object FROM `limits` WHERE id LIKE :id');
		$stmt->bindValue(':id', $limitId, SQLITE3_INTEGER);
		$result = $stmt->execute();
		$return = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$return[$row["id"]] = Limit::dec($row["object"]);
		}
		if(count($return) != 1){
			return new Limit($valueId);
		}else{
			return $return[$limitId];
		}
		
	}
	
	public static function getLimitsForValueId($valueId){
		global $db;
		$stmt = $db->prepare('SELECT id, object FROM `limits` WHERE value_id LIKE :value_id');
		$stmt->bindValue(':value_id', $valueId, SQLITE3_INTEGER);
		$result = $stmt->execute();
		$return = array();
		while($row = $result->fetchArray(SQLITE3_ASSOC)){
			$return[$row["id"]] = Limit::dec($row["object"]);
		}
		return $return;
	}
	
	private function _getUnusedId(){
		global $db;
		$stmt = $db->prepare('SELECT value_id FROM `limits` WHERE id LIKE :id');
		for($i=1;$i<10000;$i++){
			$stmt->bindValue(':id', $i, SQLITE3_INTEGER);
			$result = $stmt->execute();
			$bla = $result->fetchArray(SQLITE3_ASSOC);
			$stmt->reset();
			if($bla === false) break; 
		}
		return $i;
	}
	
	private function _save(){
		global $db;
		if($this->id != null){
			$stmt = $db->prepare('UPDATE `limits` SET `object`=:object WHERE `id`=:id');
			$stmt->bindValue(':object', $this->enc(), SQLITE3_TEXT);
			$stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
			$stmt->execute();
		}else{
			$this->id = $this->_getUnusedId();
			$stmt = $db->prepare('INSERT INTO `limits` (`id`,`value_id`,`object`) VALUES (:id,:value_id,:object)');
			$stmt->bindValue(':id', $this->id, SQLITE3_TEXT);
			$stmt->bindValue(':value_id', $this->value_id, SQLITE3_TEXT);
			$stmt->bindValue(':object', $this->enc(), SQLITE3_TEXT);
			$stmt->execute();
		}
	}
	
	/**
	 * Generates the Edit display to be used for this limit depending on the value type and the set parameters
	 * @return string A HTML String containing the elements required to define this limit and a button to attach Actions.
	 */
	function getEdit(){
		$val = Value::fromDatabase($this->value_id);
		$return = "<div><input type='hidden' name='value_id' value='".$this->value_id."' /><input type='hidden' name='limit_id' value='".$this->id."' />";
		if($val->getValueType() == "boolean"){
			//Value is a boolean, offer actions for the cases of true and false
			$return .= "<select name='limit'>";
			if($this->upperLimit === true){
				$return .= "<option value='true' selected='selected'>true</option><option value='false'>false</option>";
			}else{
				$return .= "<option value='true'>true</option><option value='false' selected='selected'>false</option>";
			}
			$return .= "</select>  <input type='button' value='Actions' onclick='editActions(this,".$this->id.");' /><input type='button' value='Delete' onclick='removeLimit(this);' />";
		}elseif($val->getValueType() == "double" || $val->getValueType() == "integer"){
			//Value is a double or integer, offer actions when the value raises above or sinks below a defined value
			$return .= "<input type='number' name='lower' value='".$this->lowerLimit."' /> < <input type='number' name='upper' value='".$this->upperLimit."' /> <input type='button' value='Actions' onclick='editActions(this,".$this->id.");' /> <input type='button' value='Delete' onclick='removeLimit(this);' />";
		}
		$return .= "<br /><span class='actions'></span>";
		$return .= "</div>";
		return $return;
	}
	
	function getValueId(){
		return $this->value_id;
	}
	
	function setLimits($lower,$upper){
		$this->lowerLimit = $lower;
		$this->upperLimit = $upper;
		$this->checkCounter = 0;
		$this->_save();
	}
	
	function getLimits(){
		return array("lowerLimit"=>$this->lowerLimit,"upperLimit"=>$this->upperLimit);
	}
	
	/**
	 * Checks the stored limits for their applicability and triggers all actions if the are applicable. 
	 * Uses a global variable $limitTriggerCounter that can be configured. The actions are only triggered if the limit was true this amount in a row 
	 */
	function checkLimits(){
		global $limitTriggerCounter;
		$val = Value::fromDatabase($this->value_id);
		$value = $val->getValue();
		$this->checkCounter++;
		$this->resetCounter++;
        if($val->getValueType() == "boolean")
        {
            if($value == $this->upperLimit)
            {
                //Limit matches
                if($this->checkCounter == $limitTriggerCounter) //Make sure to only trigger the action if the limit has reached the trigger limit
                {
                    $this->resetCounter = 0;
                    $this->performActions();

                } else
                {
                    $this->resetCounter = 0;
                    $this->_save();  //Save the progress on the checkCounter if below limit
                }
            } else
            {
                if(($this->resetCounter < $limitTriggerCounter) && ($this->checkCounter > $limitTriggerCounter))
                {
                    $this->_save();
                }

                if(($this->resetCounter == $limitTriggerCounter) || ($this->checkCounter <= $limitTriggerCounter))
                {
                    $this->checkCounter = 0;
                    $this->resetTriggers();
                }
            }
        }elseif($val->getValueType() == "double" || $val->getValueType() == "integer")
        {
            if($value <= $this->upperLimit && $value >= $this->lowerLimit)
            {
                if($this->checkCounter == $limitTriggerCounter) //Make sure to only trigger the action if the limit has reached the trigger limit
                {
                    $this->resetCounter = 0;
                    $this->performActions();

                } else
                {
                    $this->resetCounter = 0;
                    $this->_save();  //Save the progress on the checkCounter if below limit
                }
            } else
            {
                if(($this->resetCounter < $limitTriggerCounter) && ($this->checkCounter > $limitTriggerCounter))
                {
                    $this->_save();
                }

                if(($this->resetCounter == $limitTriggerCounter) || ($this->checkCounter <= $limitTriggerCounter))
                {
                    $this->checkCounter = 0;
                    $this->resetTriggers();
                }
            }
        }
    }
	
	function hasActions(){
		return (count($this->actions)>0);
	}
	
	function getActions(){
		return $this->actions;
	}
	
	function removeActions(){
		$this->actions = array();
		$this->_save();
	}

	function addAction(Action $action){
		$this->actions[] = $action;
		$this->_save();
	}
	
	function performActions(){
		foreach($this->actions as $action){
			$action->performAction();
		}
		$this->_save();
	}
	
	function resetTriggers(){
		foreach($this->actions as $action){
			$action->resetTrigger();
		}
		$this->_save();
	}
	
	public static function getAllLimits(){
		global $db;
		$stmt = $db->prepare('SELECT value_id FROM `limits`');
		$result = $stmt->execute();
		$valueIds = array();
		while($tmp = $result->fetchArray(SQLITE3_ASSOC)){
			$valueIds[] = $tmp["value_id"];
		}
		return $valueIds;
		
	}
	
	/**
	 * Encodes this object as base64
	 * @return string a base64 string encoding the original variable
	 */
	private function enc(){
		return base64_encode(serialize($this));
	}
	
	public static function dec($string){
		return unserialize(base64_decode($string));
	}
	
}
