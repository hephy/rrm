<?php

/**
 * Defines an Element that can be displayed
 */

/**
 * Class Element
 */
class Element
{
	/**
	 * @constant elementDefinitions Array containing the "view" and "edit" code for each possible element. These definitions contain the design of the elements as they are used in the edit in the layout and the view in the main display.  
	 */
 
	const elementDefinitions = array(
		"el-notification"=>array(
			"edit"=>'<span class="valueValue">&nbsp;</span><span class="valueName" title="{originalName}">{valueName}</span>',
			"view"=>'<span class="valueValue {highlight}">&nbsp;</span><span class="valueName">{valueName}</span>'
		),
		"el-value"=>array(
			"edit"=>'<span class="valueName" title="{originalName}">{valueName}</span><span class="valueValue">Value</span>',
			"view"=>'<span class="valueName">{valueName}</span><span class="valueValue" style="{color}">&nbsp;</span>'
		),
		"el-selection"=>array(
			"edit"=>'<span class="valueName" title="{originalName}">{valueName}</span><span class="valueValue"> <select><option>option</option></select></span>',
			"view"=>'<span class="valueName">{valueName}</span><span class="valueValue"> <select onchange="changeSelection(this)">{options}</select></span>'
		),
		"el-graph"=>array(
			"edit"=>'<img src="../output/rrd_scripts/{valueName}" alt="Image"/>',
			"view"=>'<img src="{valueName}" alt="Image" imgName="{valueName}"/>'
		),
		"el-logwindow"=>array(
			"edit"=>'<span class="valueName" title="{originalName}">{valueName}</span>',
			"view"=>'<div style=" width: 100%; height: 100%; border: solid 0px black; margin: 0px; padding: 0px; overflow: scroll; text-align: left; white-space: nowrap;" class="valueValue" >Loading...</div>'
		),
		"el-button"=>array(
			"edit"=>'<span class="valueName" title="{originalName}">{valueName}</span>',
			"view"=>"<input type='button' value='{valueName}' onClick='performAction($(this).parent().attr(\"target\"))' />"
		),
					);
	private $style;
	private $elementId;
	private $valueId;
	private $val;
	private $valueName;
	private $originalName;
	private $class;
	private $additionalParameters;
	
	/**
	 * Element constructor.
	 * @param int $valueId The ID of the value this element is associated with. Can also be 0 if the element is not directly assigned to a value (e.g. button, log, ...)
	 * @param string $valueName  The Name of the Element that will be displayed 
	 * @param array $additionalParameters An array containing additional parameters whose availability can vary between Elements  
	 */
	function __construct($valueId,$valueName,$additionalParameters=array())
	{
		$this->valueId = $valueId;
		$this->valueName = $valueName;
		$this->val = Value::fromDatabase($this->valueId);
		$this->originalName = $this->val->getName();
		$this->additionalParameters = $additionalParameters;
	}
	
	/**
	 * Function that replaces some placeholders with the correct values, "quick" because it doesn't fill in any options that need to be retrieved from other elements and therefore doesn't take much time.  
	 * 
	 * @param string $string
	 * @return string
	 */
	private function quickReplaceVariable($string){
		$replacementvalues = array(
			"{valueName}" =>$this->valueName,
			"{originalName}" => $this->originalName,
		);
		foreach($replacementvalues as $variable=>$value){
			$string = str_replace($variable,$value,$string);
		}
		return $string;
	}
	
	/**
	 * Function that replaces the placeholders that take longer to gather since they need to be retrieved from external objects.   
	 * 
	 * @param string $string A string containing variables in the {name} format that shall be replaced with their real values
	 * @return string A string that hopefully had all the variables replaced
	 */
	private function replaceVariables($string){
		$this->val = Value::fromDatabase($this->valueId);
		$string = $this->quickReplaceVariable($string);
		$replacementvalues = array(
			"{highlight}" => "", //$this->getHighlight(),  //Removed this as it takes forever during loading and is overwritten with the dynamic values instantly right afterwards
			"{options}" =>$this->getOptions(),
			"{color}" => $this->getColor()
		);
		foreach($replacementvalues as $variable=>$value){
			$string = str_replace($variable,$value,$string);
		}
		return $string;
	}
	
	private function getAdditionalParameters(){
		$return = "";
		foreach($this->additionalParameters as $name=>$parameter){
			$return .= $name."='".$parameter."' ";
		}
		return $return;
	}
	
	/**
	 * Returns an option list of the parameter names. Required for drop-down menus
	 * @return string
	 */
	private function getOptions(){
		$return = "";
		foreach ($this->val->getAllValueNames() as $id=>$name){
			$return .= "<option value='$id'>$name</option>";
		}
		return $return;
	}
	
	/**
	 * Returns the current highlight definition of the element.  
	 * @return string
	 */
	private function getHighlight(){
		$highlight = $this->val->getNameForValue($this->val->getValue());
		return $highlight;
	}
	
	/**
	 * Return the colors of the element (text and background) as formatted css parameters
	 * @return string
	 */
	public function getColor(){
		$return = "";
		foreach($this->val->getColors() as $name=>$color){
			$return .= $name.": ".$color.";";
		}
		return $return ;
	}
	
	function setStyle($style){
		$this->style = $style;
	}
	
	function setClass($class){
		$this->class = $class;
	}
	
	function setName($name){
		$this->valueName = $name;
	}
	
	function setElementId($id){
		$this->elementId = $id;
	}
	
	/**
	 * Returns the element in its edit view that allows for editing in the interface
	 * @return string String containing the complete output
	 */
	public function getEditView(){
		foreach(explode(" ",$this->class) as $class){
			if(@is_array($this::elementDefinitions[$class])){
				$content = $this->quickReplaceVariable($this::elementDefinitions[$class]["edit"]);
			}
		}
		if(!isset($content)){
			debug("missing definition for at least one of these classes: ".$this->class);
			return "";
		}
		return '<div class="'.$this->class.'" style="'.$this->style.'" value_id="'.$this->valueId.'" value_name="'.$this->valueName.'" id="element_'.$this->elementId.'" '.$this->getAdditionalParameters().'>'.$content.'</div>';
	}
	
	/**
	 * Returns the Element in its view view that is intended for being displayed in a completed layout
	 * @param int $width Width of the layout (Used for resizing, currently not used)
	 * @param int $height Height of the layout (Used for resizing, currently not used)
	 * @return string String containing the complete output
	 */
	public function getDisplayView($width,$height){
//		$tmp = explode(";",$this->style);
//		$css = array();
//		foreach($tmp as $setting){
//			$tmp2 = array_map("trim",explode(":",$setting));
//			if($tmp2[0] != "") $css[$tmp2[0]] = $tmp2[1];
//		}
		//Convert pixel dimensions to percentages for viewing
//		$css["width"] = "auto"; //(str_replace("px","",$css["width"])*100/$width)."%";
//		$css["left"] = "0px";//(str_replace("px","",$css["left"])*100/$width)."%";
//		$css["top"] = (str_replace("px","",$css["top"])*100/$height)."%";
//		$css["height"] = (str_replace("px","",$css["height"])*100/$height)."%";
		
//		$style = "";
//		foreach($css as $key=>$value){
//			$style .= $key.":".$value.";";
//		}
		$style = $this->style;
		$this->class = str_replace("ui-draggable","",str_replace("ui-draggable-handle","",$this->class));  //Remove the draggable handle
		

		
		foreach(explode(" ",$this->class) as $class){
			if(@is_array($this::elementDefinitions[$class])){
				$content = $this->replaceVariables($this::elementDefinitions[$class]["view"]);
			}
		}
		if(!isset($content)){
			debug("missing definition for at least one of these classes: ".$this->class);
			return "";
		}
		
		return '<div class="'.$this->class.'" style="'.$style.'" value_id="'.$this->valueId.'" value_name="'.$this->valueName.'" '.$this->getAdditionalParameters().'>'.$content.'</div>';
	}
	
	/**
	 * Encodes this object as base64
	 * @return string a base64 string encoding the original variable
	 */
	public function enc(){
		return base64_encode(serialize($this));
	}
	
	public static function dec($obj){
		return unserialize(base64_decode($obj));
	}
}
