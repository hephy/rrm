<?php

/**
 * Configuration class providing a possibility to load and store graph configurations for the rrd database. 
 */

/**
 * Class GraphConfiguration
 */
class GraphConfiguration
{
	const defaultSettings = "";
	
	/**
	 * Saves the configuration to the database
	 * @param int $id ID of the Graph
	 * @param string $name Name of the Graph
	 * @param string $title Title of the Graph (is displayed in the graph)
	 * @param string $vertical Vertical axis label
	 * @param int $width Width of the graph
	 * @param int $height Height of the graph
	 * @param string $configuration Configuration string containing the assignment of all variables
	 * @return string
	 */
	public function saveConfiguration($id,$name,$title,$vertical,$width,$height,$configuration){
		global $db;
		//Save to Database
		$stmt = $db->prepare("SELECT id from `graphs` WHERE `id` = :id");
		$stmt->bindValue(':id',$id);
		$result = $stmt->execute();
		$graphs = array();
		while ($graph = $result->fetchArray(SQLITE3_ASSOC)) {
			$graphs = $graph["id"];
		}
		//Check if ID exists
		if(count($graphs)>0){
			//ID exists - UPDATE
			$stmt = $db->prepare('UPDATE `graphs` SET  `name`=:name, `configuration`=:configuration, `title`=:title, `vertical`=:vertical, `width`=:width, `height`=:height WHERE `id`=:id');
			
		}else{
			//ID doesn't exist - INSERT
			$stmt = $db->prepare('INSERT INTO `graphs` (name,title,vertical,width,height,configuration) VALUES(:name,:title,:vertical,:width,:height,:configuration)');
		}
		
		$stmt->bindValue(':id', $id, SQLITE3_INTEGER);
		$stmt->bindValue(':name', $name, SQLITE3_TEXT);
		$stmt->bindValue(':title', $title, SQLITE3_TEXT);
		$stmt->bindValue(':vertical', $vertical, SQLITE3_TEXT);
		$stmt->bindValue(':width', $width, SQLITE3_INTEGER);
		$stmt->bindValue(':height', $height, SQLITE3_INTEGER);
		$stmt->bindValue(':configuration', $configuration, SQLITE3_TEXT);
		$stmt->execute();
		
		return json_encode(array("error"=>"false"));
	}

    /**
     * Delete the configuration from the database
     * @param int $id ID of the Graph
     * @return string
     */
    public function deleteConfiguration($id){
        global $db;
        //Delete from Database
        $stmt = $db->prepare("DELETE from `graphs` WHERE `id` = :id");
        $stmt->bindValue(':id',$id);
        $stmt->execute();

        return json_encode(array("error"=>"false"));
    }
	
	/**
	 * Replaces placeholders in the code
	 * 
	 * @param string $configuration configuration string where placeholders need to be replaced
	 * @return string 
	 */
	private function replacePlaceholders($configuration){
		global $rrdDatabases;
		$return = str_replace("{rrdDatabases}",$rrdDatabases,$configuration);
		return $return;
	}
	
	/**
	 * Returns a rrdtool graph command according to the given parameters 
	 * @param int $id ID of the Graph
	 * @param int $startTime Time where the first datapoint of the graph should be taken from (use negative values to go to the past relative to the current time)
	 * @param string $title Title of the Graph (is displayed in the graph)
	 * @param string $vertical Vertical axis label
	 * @param int $width Width of the graph
	 * @param int $height Height of the graph
	 * @param string $configuration Configuration string containing the assignment of all variables
	 * @param bool $temp Defines if the configuration should be created as temporary (with different title and without the output being suppressed)
	 * @return string
	 */
	public function createConfiguration($id,$startTime,$title="Title",$vertical="",$width=800,$height=200,$configuration="",$temp=true){
		global $rrdScripts;
		if(is_array($id)){
			$conf = $id;
			$id = $conf["id"];
			$title = $conf["title"];
			$vertical = $conf["vertical"];
			$width = $conf["width"];
			$height = $conf["height"];
			$configuration = $conf["configuration"];
			$temp = false;
			if(isset($conf["timeSuffix"])){
				$timeSuffix = $conf["timeSuffix"];
			}else{
				$timeSuffix = "";
			}
		}
		$configuration = trim($this->replacePlaceholders($configuration));
		if($temp){
			$string = 'rrdtool graph '.$rrdScripts.'temp_'.$id.'.png -a PNG --start '.$startTime.' -t "'.$title.' (Temporary File)" --vertical-label "'.$vertical.'" -w '.$width.' -h '.$height.' '.$configuration;
		}else{
			$string = 'rrdtool graph '.$rrdScripts.'graph_'.$id."_".$timeSuffix.'.png -a PNG --start '.$startTime.' -t "'.$title.' ('.$timeSuffix.')" --vertical-label "'.$vertical.'" -w '.$width.' -h '.$height.' '.$configuration.' > /dev/null';
		}
		
		return $string;
	}
	
	/**
	 * Checks the given configuration options and returns an array containing the result and if possible the file name of the generated temporary file
	 * 
	 * @param int $id ID of the Graph
	 * @param int $startTime Time where the first datapoint of the graph should be taken from (use negative values to go to the past relative to the current time)
	 * @param string $title Title of the Graph (is displayed in the graph)
	 * @param string $vertical Vertical axis label
	 * @param int $width Width of the graph
	 * @param int $height Height of the graph
	 * @param string $configuration Configuration string containing the assignment of all variables
	 * @return array
	 */
	public function checkConfiguration($id,$startTime,$title,$vertical,$width,$height,$configuration){
		$tempCommand = $this->createConfiguration($id,$startTime,$title,$vertical,$width,$height,$configuration,true);
		$reply = shell_exec($tempCommand." 2>&1");
		
		if(preg_match("(\d*x\d*)",$reply)==0){ 
			return array("error"=>true,"msg"=>$reply);
		}else{
			return array("error"=>false,"fileName"=>'temp_'.$id.'.png');
		}
	}
	
	/**
	 * Returns the configuration interface for the given parameters and if necessary an empty one. 
	 * 
	 * @param array $confData
	 * @return string
	 */
	public function getEditView($isNew, $confData=array()){
		$data = array("id"=>0,"name"=>"","title"=>"Title","vertical"=>"","width"=>800,"height"=>200,"configuration"=>"");  //Default values

		foreach($confData as $name=>$value){  //If configuration data is set assign it to the values. 
			$data[$name] = $value;
		}
		$return = "<div class='graphConfiguration' style='border: solid 1px black; margin-bottom:10px;'>";
		$return .= "<input type='hidden' value='".$data["id"]."' name='id'/>";
		$return .= "<input type='text' value='".$data["name"]."' name='name' placeholder='Name'/>";
		$return .= "<input type='text' value='".$data["title"]."' name='title' placeholder='Graph Title'/>";
		$return .= "<input type='text' value='".$data["vertical"]."' name='vertical' placeholder='Vertical Label'/>";
		$return .= "<input type='number' value='".$data["width"]."' name='width' placeholder='Graph Width'/>";
		$return .= "<input type='number' value='".$data["height"]."' name='height' placeholder='Graph Height'/>";
		$return .= "<input type='button' value='Preview' onclick='previewGraph(this);' />";
        if($isNew)
        {
            $return .= "<input type='button' value='Save' onclick='saveGraph(this); setTimeout(function(){ window.location.reload(); }, 500);' />";
        }
        else
        {

            $return .= "<input type='button' value='Save' onclick='saveGraph(this); setTimeout(function(){ window.location.reload(); }, 750);' />";
            $return .= '<input type="button" value="Delete" onclick="if(confirm(\'Are you sure you want to delete this?\')) deleteGraph(this), setTimeout(function(){ window.location.reload(); }, 500);" />';
        }
		$return .= "<br /><textarea name='configuration' cols='100' rows='20' style='margin-right:30px;'>".$data["configuration"]."</textarea>";
		$return .= "<div class='preview' ></div>";
		$return .= "</div>";
		return $return;
	}
}
