<?php
/**
 * Created by PhpStorm.
 * User: Benedikt Würkner
 * Date: 21/03/16
 * Time: 15:48
 */

error_reporting(E_ALL);
ini_set('display_errors', true);
$startTime = microtime(true);
$globalStartTime = $startTime;


$basePath = "/var/www/html/";

$oneWireIp = "localhost";
$knxIP = "localhost";

$limitTriggerCounter = 3; //The number of times a value needs to consecutively be above the defined limit for the actions to be triggered

//File Locations
$databaseFile = $basePath."conf/rrm.sqlite";
$rrdDatabases = $basePath."output/rrd_databases/";
$rrdScripts   = $basePath."output/rrd_scripts/";

$graphsTimeframes = array("6h"=>(3600*6),"36h"=>(3600*36),"1w"=>(3600*24*7),"1m"=>(3600*24*31),"1y"=>(3600*24*365));

//SQLite3 Database configuration
$db = new SQLite3($databaseFile);
$db->busyTimeout(500);
$db->exec('PRAGMA journal_mode = wal;');

require "../lib/Logger.php";

$defaultLogger = new Logger($basePath."default.log",$db,true,true,"default");

//OneWire connection
define('USE_1WIRE_VALUE_HANDLER', true);
define('GET_VALUE_FROM_OWFS', false);
define('SET_OWFS_DIRECTORY_ADDRESS', "/mnt/1wire");

require "../lib/DataSources/OneWire.php";
$oneWire = new OneWire($oneWireIp);

//Error handling on OneWire connection
define('ERROR_LOGGER_SET', false);
define('NUMBER_OF_RETRY_ON_ERROR', 5);

//GPIO connection
require "../lib/DataSources/GpioPHP.php";
$gpio = new GpioPHP();

//KNX connection
require "../lib/DataSources/KNX.php";
$knx = new KNX($knxIP);

//Parameter configuration
require "../lib/DataSources/Value.php";

$GLOBALS["log"] = array();
//Globally available functions

function runtime($descriptor,$return=false){
	global $startTime;
	$now = microtime(true)-$startTime;
	$startTime = microtime(true);
	if($return){
		return $descriptor." took ".sprintf("%1.2f seconds",$now);
	}else{
		echo $descriptor." took ".sprintf("%1.2f seconds",$now)."<br />";
	}
}

function formatBytes($bytes){
	$bytes = (int) $bytes;
	
	if ($bytes > 1024*1024) {
		return round($bytes/1024/1024, 2).' MB';
	} elseif ($bytes > 1024) {
		return round($bytes/1024, 2).' KB';
	}
	
	return $bytes . ' B';
}

function debug($var){
	echo "<pre>";
	var_dump($var);
	echo "</pre>";
}

function generateSelect($selectName,$elements,$currSelection=""){
	$return = "";
	$return .= "<select name='".$selectName."'>";
	foreach($elements as $name=>$label){
		$selected = ($currSelection == $name)?"selected='selected'":"";
		$return .=  "<option value='".$name."' ".$selected.">".$label."</option>";
	}
	$return .= "</select>";
	
	return $return;
}

function renderLayout($isNew, $layout_id){
    global $db;

    $stmt = $db->prepare('SELECT id,name,position FROM `layouts` WHERE id LIKE :id');
    $stmt->bindValue(':id', $layout_id, SQLITE3_INTEGER);
    $result = $stmt->execute();
    $layout = $result->fetchArray(SQLITE3_ASSOC);
    $deleteLayout = '<br />';

    if(!$isNew)
    {
        $deleteLayout = '<input type="button" value="Delete" onclick="if(confirm(\'Are you sure you want to delete this?\')) deleteLayout($(this).parent()), setTimeout(function(){ window.location.reload(); }, 500);" /><br />';
    }

    echo '<div id="l'.$layout_id.'">
			<input type="text" value="'.$layout["name"].'" name="layoutName" /> 
			<input type="button" value="Save" onclick="saveLayout($(this).parent());" />
			<input type="button" value="Preview" onclick="previewLayout($(this).parent());" />
			' . $deleteLayout . '
			<div id="layout_'.$layout_id.'" class="layout">';

	$stmt = $db->prepare('SELECT id,layout_id,name,position FROM `groups` WHERE layout_id LIKE :layout_id');
	$stmt->bindValue(':layout_id', $layout_id, SQLITE3_INTEGER);
	$result = $stmt->execute();
	while ($group = $result->fetchArray(SQLITE3_ASSOC)) {
		$g = new Group($group["name"],$group["position"]);
		$g->setGroupId($group["id"]);
		//Get all elements for this group
		$stmt = $db->prepare('SELECT id,name,group_id,display_object FROM `elements` WHERE group_id = :group_id');
		$stmt->bindValue(':group_id', $group["id"], SQLITE3_INTEGER);
		$elements = $stmt->execute();
		while ($element = $elements->fetchArray(SQLITE3_ASSOC)) {
			$g->addElement(unserialize(base64_decode($element["display_object"])));
		}
		echo $g->getEditView();
	}
	echo "</div>";
	echo "</div>"; //Close #l{number}
	
}
