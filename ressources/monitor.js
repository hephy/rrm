/**
 * Created by Benedikt Würkner on 03.06.2016.
 * 
 * Contains all the Javascript required for the layout output and interaction like automatic loading of data, actions of buttons, changing of colors and so on
 */
if(basePath == null){
    var basePath = "";
}
if(updateInterval == null){
    var updateInterval = 10*5*1000;
}

//Functions for Value Elements
function loadValue(elem){
    $.post(basePath+"requestHandler.php", {
        "r": "getValueById",
        "id": elem.attr("value_id")
    },function(data){
        // console.log(data);
        elem.find("span.valueName").html(elem.attr("value_name"));
        elem.find("span.valueValue").html(data.value);
        elem.find("span.valueValue").attr("rawValue",data.rawValue);
        elem.find("span.valueValue").attr("style",data.colors);
        window.setTimeout(loadValue.bind(null,elem), updateInterval);
    },"json")
}
//On click of an input element replace with a text field containing the raw value. On loosing focus replace again with span and reload the value
function showInputBox(elem){
    var span = $(elem.target);
    var parent = span.parent();
    span.replaceWith("<input type='number' value='"+span.attr("rawValue")+"' size='"+(span.attr("rawValue").length+2)+"' value_id='"+parent.attr("value_id")+"'/>");
    var input = parent.find("input");
    input.attr("class",span.attr("class"));
    input.focus();
    input.focusout(function(){
        setValue(input.attr("value_id"),input.val()); //Send the newly set value to the server
        input.replaceWith(span);  //Put the previous "Span" element in there again
        span.html("loading...");  //Replace the value with loading until the new value is loaded
        loadValue(span.parent()); //Load the current value
        span.click(showInputBox);  //Reattach the click handler
    });
    $(this).unbind("click");  //Detach the click handler

}

function checkImage(elem){
    var img = $(elem).find("img"); 
    var d = new Date();
    img.attr("src",graphPath+img.attr("imgName")+"?"+d.getTime());
    window.setTimeout(checkImage.bind(null,elem), updateInterval);
}

//Functions for Notification Elements
function checkNotification(elem){
    $.post(basePath+"requestHandler.php", {
        "r": "getValueById",
        "id": elem.attr("value_id")
    },function(data){
        // console.log(data);
        $(elem).find(".valueValue").attr("class","valueValue "+data.valueName);
        $(elem).find(".valueValue").attr("value",data.rawValue);
        window.setTimeout(checkNotification.bind(null,elem), updateInterval);
    },"json")
}

//Functions for Notification Elements
function checkSelection(elem){
    $.post(basePath+"requestHandler.php", {
        "r": "getValueById",
        "id": elem.attr("value_id")
    },function(data){
        elem.find("select").val(data.rawValue);
        window.setTimeout(checkSelection.bind(null,elem), updateInterval);
    },"json")
}

//Functions for Notification Elements
function checkLog(elem){
    $.post(basePath+"requestHandler.php", {
        "r": "getLog",
        "loglevel": elem.attr("target"),
        "rows" : elem.attr("rows")
    },function(data){
        var content = "<table>";
        var listTime = Math.floor(((new Date().valueOf()/1000/3600))/24);
        var timeOffset = new Date().getTimezoneOffset()/60*-1;
        $.each(data,function(i,j){
            while(Math.floor((j["timestamp"]/3600+timeOffset)/24)<listTime){
                content += "<tr><td>----</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
                listTime = listTime-1;
            }
            content += "<tr><td style='white-space: nowrap'>"+formatDate(j["timestamp"])+"</td><td>"+j["levelName"]+"</td><td style='white-space: nowrap'>"+j["message"]+"</td></tr>";
        });
        content += "</table>";
        elem.find(".valueValue").html(content);
        window.setTimeout(checkLog.bind(null,elem), updateInterval);
    },"json")
}

function changeSelection(elem){
    setValue($(elem).parent().parent().attr("value_id"),$(elem).val());
}

// Functions for Buttons
function increaseValue(value_id,increment){
    
}

function decreaseValue(value_id,increment){
    
}

function performAction(target){
    document.location.href=target;
}

function setValue(value_id,value){
    $.post(basePath+"requestHandler.php", {
        "r": "setValue",
        "value_id": value_id,
        "valueValue": value
    },function(data){
        console.log(data);
    })
}

function loadAllValues(target){
    if(target == undefined){
        target = "#monitor";
    }
    $(target+" .el-value").each(function(){
        loadValue($(this)); //Only load values for non-input type values
    });
    $(target+" .el-notification").each(function(){
        checkNotification($(this));
    });
    $(target+" .el-selection").each(function(){
        checkSelection($(this));
    });
    $(target+" .el-graph").each(function(){
        checkImage($(this));
    });
    $(target+" .el-logwindow").each(function(){
        checkLog($(this));
    });
}

function loadLayout(layout_id){
    $.post(basePath+"requestHandler.php", {
        "r": "getLayout",
        "layout_id": layout_id
    }, function (data) {
        var delay = 300;
        $("#monitor").html(data);
        // $("#monitor").find(".group").each(function(i,j){
        //     $(j).hide().fadeIn(delay);
        // });
        loadAllValues();
        $("#monitor .el-value.el-input").each(function(){
            $(this).click(showInputBox);
        });
        $("#monitor .el-notification.el-input").each(function(){
            $(this).click(toggleSwitch);
        });
    });    
}

//Functions for Booleans

function toggleSwitch(elem){
    var value_id = $(elem.toElement).parent().attr("value_id");
    var value = ($(elem.toElement).attr("value")==0)?1:0;
    $(elem.toElement).animate({"border-width":"1px"},200).delay(200).animate({"border-width":"2px"},200);
    $.post(basePath+"requestHandler.php", {
        "r": "setValue",
        "value_id": value_id,
        "valueValue": value
    },function(data){
        console.log(data);
    })
    
}

//Functions for Graphs


//Functions for Log elements
function formatDate(unixTimestamp) {

    dt = new Date(unixTimestamp*1000); //*1000 because of different format in javascript *facepalm*
    var hours = dt.getHours();
    var minutes = dt.getMinutes();
    var seconds = dt.getSeconds();

    var day = dt.getDate();
    var month = dt.getMonth()+1;
    var year = dt.getFullYear();

    switch(dt.getDay()){
        case 0:
            var wkday = "Sun";
            break;
        case 1:
            var wkday = "Mon";
            break;
        case 2:
            var wkday = "Tue";
            break;
        case 3:
            var wkday = "Wed";
            break;
        case 4:
            var wkday = "Thu";
            break;
        case 5:
            var wkday = "Fri";
            break;
        case 6:
            var wkday = "Sat";
            break;
    }
    // the above dt.get...() functions return a single digit
    // so I prepend the zero here when needed
    if (hours < 10)
        hours = '0' + hours;

    if (minutes < 10)
        minutes = '0' + minutes;

    if (seconds < 10)
        seconds = '0' + seconds;

    return wkday+", "+day+"."+month+"."+year+" "+hours + ":" + minutes + ":" + seconds;
};
