# README #

RRM - the HEPHY "Rein Raum Monitor" is a PHP program to interactively show the status of GPIO Pins of a Raspberrry PI, various OneWire-Sensor data and KNX parameters onto a dynamically generated, and periodically updated webpage.

### How do I get set up? ###

#### install required packages: ####
    #one wire filesystem including documentation
    sudo apt-get install owfs owfs-doc -y 
    #open source firefox as browser
    sudo apt-get install iceweasel -y
    #database
    sudo apt-get install sqlite3  -y
    #webserver
    sudo apt-get install php5-pecl-http php5 php5-rrd php5-sqlite apache2 -y
    #rrdtool
    sudo apt-get install rrdtool -y
    #all in one
    sudo apt-get install owfs owfs-doc  iceweasel sqlite3 php5-pecl-http php5 php5-rrd php5-sqlite apache2 rrdtool -y



Switch to the html folder of the apache installation (default /var/www/html/) and check out the repository. Use the code below for a recommended setup
    
    cd /var/www/html/
    sudo mkdir rrm
    sudo chown www-data:pi rrm
    sudo chmod g+w rrm
    git clone https://bitbucket.org/hephy/rrm.git
    cd rrm
    mkdir -p output/rrd_databases
    mkdir -p output/rrd_scripts
    cd conf
    cp default.conf.php conf.php
    cp default.rrm.sqlite rrm.sqlite
    vim conf.php
    
Adapt the paths and values as required by your project in the now open config file

Move your browser to http://localhost/rrm/admin

#### Graphs ####

The graphs are created from RRD which in turn need to be filled with data regularly. To this end the "loop.py" skript in the root directory needs to be executed as often as desired. The default interval for data is 20 seconds with a maximum deadtime of 40 seconds (heartbeat). 
Example shell script that can be used to achive this goal
 
    #!/bin/sh
    while true
    do 
        python /var/www/html/loop.py
        sh /var/www/html/output/rrd_scripts/graphs_6h.sh
        sleep 5
    done

The definition of the lengths of graph intervals can be found in the configuration file. default are 6h,36h,1week,1month,1year 

For automatic creation of graphs in a regular interval a symbolic link needs to be placed in the corresponding directories of cron (/etc/cron.daily, /etc/cron.hourly, ...) to the graph script. 

For the python script one needs to install pyRRD from https://pypi.python.org/pypi/PyRRD/ (download and then execute python setup.py install)
    
    wget https://pypi.python.org/packages/6c/fb/c2717024b02a18ddf7b7484facb9b47e2efd8e052ff5fd73c5a2185e0277/PyRRD-0.1.0.tar.gz
    tar xzf PyRRD-0.1.0.tar.gz 
    cd PyRRD-0.1.0/
    sudo python setup.py install


#### 1Wire configuration ####
    
Check the /etc/owfs.conf configuration file, make sure to comment 
    
    # server: FAKE = DS18S20,DS2405
and uncomment
    
    server: usb = all
    
Also because of past experiences, make sure the server port definition looks like this
    
    server: port = 4304
    
#### KNX ####

If you want to use KNX you have to install the knx-Deamon as explained here: https://github.com/knxd/knxd



